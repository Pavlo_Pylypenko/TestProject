﻿using System.Collections.Generic;
using System.Linq;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Helpers
{
    public static class RoleHelper
    {
        public const string Root = "Root Account";
        public const string Admin = "IT Admin";
        public const string RegionalManager = "Regional Manager";
        public const string StoreManager = "Store Manager";
        public const string User = "User Account";

        public static Dictionary<int, string> Roles { get; set; }

        static RoleHelper()
        {
            Roles = new Dictionary<int, string> {{1, Root}, {2, Admin}, {3, RegionalManager}, {4, StoreManager}, {5, User}};
        }

        public static IEnumerable<Role> GetLessRoles(string maxRole)
        {
            var max = Roles.FirstOrDefault(r => r.Value == maxRole);
            for (int i = max.Key + 1; i <= 5; i++)
            {
                var role = new Role {Id = i, Name = Roles[i]};
                yield return role;
            }
        }

        internal static bool IsLessRole(string role, string roleLess)
        {
            return Roles.FirstOrDefault(r => r.Value == role).Key < Roles.FirstOrDefault(r => r.Value == roleLess).Key;
        }
    }
}
