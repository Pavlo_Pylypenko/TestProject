﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace Detego.CatWalk.BusinessLayer.Helpers
{
    public static class IdentityExtension
    {
        public static int GetId(this IPrincipal principal)
        {
            if (principal.Identity != null)
            {
                var identity = principal.Identity as ClaimsIdentity;
                int id = int.Parse(identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                return id;
            }

            return 0;
        }

        public static int? GetTenantId(this IPrincipal principal)
        {
            if (principal.Identity != null)
            {
                var identity = principal.Identity as ClaimsIdentity;
                var tenantClaim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.UserData);
                if (tenantClaim != null)
                    if (string.IsNullOrEmpty(tenantClaim.Value))
                        return null;
                    else
                        return int.Parse(tenantClaim.Value);
            }
            return null;
        }

        public static string GetRole(this IPrincipal principal)
        {
            if (principal.Identity != null)
            {
                var identity = principal.Identity as ClaimsIdentity;
                string role = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
                return role;
            }

            return "";
        }
    }
}
