﻿using System.Configuration;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Helpers
{
    public static class EmailHelper
    {
        private static SmtpSection _smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

        public static async void SendPasswordReset(User user, string site)
        {
            string resetPasswordLink = string.Format(@"{0}/ChangePassword?email={1}&token={2}", site, user.Email, user.Password);
            var smtp = new SmtpClient();
            
            var from = _smtpSection.Network.UserName;
            var to = user.Email;

            var mail = new MailMessage();
            mail.From = new MailAddress(from, "Detego");

            mail.Subject = "Resetting your Detego CatWalk password";
            StringBuilder body = new StringBuilder();
            body.AppendLine(string.Format("Dear {0}", user.UserName));
            body.AppendLine("");
            body.AppendLine("You (or somebody else) requested to reset the password for your Detego CatWalk user account.");
            body.AppendLine("If you don't want  to reset the password, you can simply ignore this email.");
            body.AppendLine("");
            body.AppendLine(string.Format("To reset your password, please use this link: {0}", resetPasswordLink));
            body.AppendLine("Sincerely,");
            body.AppendLine("Detego CatWalk");
            mail.Body = body.ToString();

            mail.To.Add(new MailAddress(to, user.UserName));

            InitializeSmtp(smtp);

            smtp.SendCompleted += (s, e) =>
            {
                smtp.Dispose();
            };

            await smtp.SendMailAsync(mail);
        }

        public static async void SendCreateUser(User user)
        {
            var smtp = new SmtpClient();

            var from = _smtpSection.Network.UserName;
            var to = user.Email;

            var mail = new MailMessage();
            mail.From = new MailAddress(from, "Detego");

            mail.Subject = "New user created";
            StringBuilder body = new StringBuilder();
            body.AppendLine(string.Format("Dear {0}", user.UserName));
            body.AppendLine("");
            body.AppendLine("A new account has been created for you at Detego CatWalk,");
            body.AppendLine("and you have been issued with as new temporary password.");
            body.AppendLine("");
            body.AppendLine("Your current login information is:");
            body.AppendLine(string.Format("login: {0} ", user.Email));
            body.AppendLine(string.Format("password: {0} ", user.Password));
            body.AppendLine("(you will have to change your password when you login for the first time)");
            body.AppendLine("");
            body.AppendLine("Sincerely,");
            body.AppendLine("Detego CatWalk");
            mail.Body = body.ToString();

            mail.To.Add(new MailAddress(to, user.UserName));
            
            InitializeSmtp(smtp);

            smtp.SendCompleted += (s, e) =>
            {
                smtp.Dispose();
            };
            await smtp.SendMailAsync(mail);
        }

        public static async void SendPasswordChanged(User user, string newPassword)
        {
            var smtp = new SmtpClient();

            var from = _smtpSection.Network.UserName;
            var to = user.Email;

            var mail = new MailMessage();
            mail.From = new MailAddress(from, "Detego");

            mail.Subject = "Your password has been changed";
            StringBuilder body = new StringBuilder();
            body.AppendLine(string.Format("Dear {0}", user.UserName));
            body.AppendLine("");
            body.AppendLine("Password has been changed for you at Detego CatWalk.");
            body.AppendLine("");
            body.AppendLine("Your current login information is:");
            body.AppendLine(string.Format("login: {0} ", user.Email));
            body.AppendLine(string.Format("password: {0}.", newPassword));
            body.AppendLine("");
            body.AppendLine("Sincerely,");
            body.AppendLine("Detego CatWalk");
            mail.Body = body.ToString();

            mail.To.Add(new MailAddress(to, user.UserName));
            InitializeSmtp(smtp);

            smtp.SendCompleted += (s, e) =>
            {
                smtp.Dispose();
            };
            await smtp.SendMailAsync(mail);
        }

        private static void InitializeSmtp(SmtpClient smtp)
        {
            smtp.Host = _smtpSection.Network.Host;
            smtp.Port = _smtpSection.Network.Port;
            smtp.Credentials = new NetworkCredential(_smtpSection.Network.UserName, _smtpSection.Network.Password);
            smtp.EnableSsl = true;
        }

    }
}
