﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Transactions;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Helpers
{
    public class EntityFrameworkHelper
    {
        public static void BulkSave<T>(List<T> entities, string tableName) where T : class
        {
            DetegoCatWalkDbContext context = new DetegoCatWalkDbContext();
            using (var connection = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    using (var sbCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                    {
                        sbCopy.BulkCopyTimeout = 0;
                        sbCopy.BatchSize = entities.Count;
                        sbCopy.DestinationTableName =tableName;
                        var reader = entities.AsDataReader();
                        sbCopy.WriteToServer(reader);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    NLogLogger logger = new NLogLogger(null, @"Entitites BulkSave");
                    logger.Error("BulkSave was not completed, reason: " + NLogLogger.GetException(ex).Message);
                    transaction.Rollback();
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }
        }

        public static void Save<T>(List<T> entities) where T : class
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(1, 30, 0)))
            {
                DetegoCatWalkDbContext context = null;
                try
                {
                    context = new DetegoCatWalkDbContext();
                    context.Configuration.AutoDetectChangesEnabled = false;

                    int count = 0;
                    foreach (var entityToInsert in entities)
                    {
                        ++count;

                        //Debug.WriteLine("count={0}, {1}", count, HiResDateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture));
                        context = AddToContext(context, entityToInsert, count, 100, true);
                    }

                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    NLogLogger logger = new NLogLogger(null, @"Entitites Save");
                    logger.Error("Save was not completed, reason: " + NLogLogger.GetException(ex).Message);
                }
                finally
                {
                    if (context != null)
                        context.Dispose();
                }

                scope.Complete();
            }
        }

        private static DetegoCatWalkDbContext AddToContext<T>(DetegoCatWalkDbContext context, T entity, int count, int commitCount, bool recreateContext) where T : class
        {
            context.Set<T>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new DetegoCatWalkDbContext();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }
    }
}
