﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Detego.CatWalk.BusinessLayer.Helpers
{
    public class PasswordHelper
    {
        public static string CalculateMD5Hash(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        public static string RandomPassword()
        {
            var random = new Random();
            var builder = new StringBuilder();
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqstuvwxyz";
            const string numbers = "0123456789";
            for (int i = 0; i < 7; i++)
                builder.Append(letters[random.Next(letters.Length - 1)]);
            builder.Append(numbers[random.Next(numbers.Length - 1)]);

            return builder.ToString();
        }
    }
}
