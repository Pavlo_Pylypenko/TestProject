﻿using System;

namespace Detego.CatWalk.BusinessLayer.Helpers
{
    /// <summary>
    /// Summary description for EPC
    /// </summary>
    public class EpcHelper
    {
        #region Constantes

        // EPC header values
        public const string SGTIN96Header = "00110000";
        public const string SGTIN64Header = "10";
        public const string SSCC96Header = "00110001";
        public const string SSCC64Header = "00001000";

        // URI headers
        public const string SGTIN96Uri = "urn:epc:tag:sgtin-96:";
        public const string SSCC96Uri = "urn:epc:tag:sscc-96:";
        public const string SGTIN64Uri = "urn:epc:tag:sgtin-64:";
        public const string SSCC64Uri = "urn:epc:tag:sscc-64:";


        // ErrorMessages
        public const string INV_HEX_DIGIT = "This is not a valid Hexadecimal number - Unexpected digit [{0}] found";
        public const string INV_EPC_PARTITION = "Invalid partition value [{0}] found in EPC. Partition value has to be a number between 0 adn 6";
        public const string INV_EPC_LENGTHMODE = "Invalid partition length mode - supported values are Bits and Digits";
        public const string DB_NO_CONNECTION = "DB Error: Unable to connect to database";
        public const string DB_PRODUCT_NOT_FOUND = "{0} [{1}] does not exist in database";

        #endregion

        #region Metodos Publicos



        /// <summary>
        /// convert EPC to EAN13
        /// </summary>
        /// <param name="epc"></param>
        /// <returns></returns>
        public string GetEAN13(string epc)
        {
            try
            {
                epc = epc.Replace("0x", "");
                string men;
                string ean = this.Decode(epc, BarcodeType.GTIN, out men);

                ean = ean.Substring(1, 12);
                return ean + this.CalcularDigitoControl("EAN13", ean);

            }
            catch (Exception)
            {
                //throw new Exception("Error converting EPC to EAN13!");
                return null;
            }

        }


        #endregion

        #region Decode
        /// <summary>
        /// Decodes an EPC number to its UPC equivalent. Currently supports only GTIN
        /// </summary>
        /// <param name="hexEPC">Hexadecimal EPC encoded number</param>
        /// <param name="targetType">Required barcode type</param>
        /// <returns>Barcode</returns>    
        public string Decode(string hexEPC, BarcodeType targetType, out string errorMessage)
        {
            errorMessage = null;
            if (hexEPC.Length == 0)
            {
                errorMessage = "EPC cannot be blank";
                return null;
            }
            string binaryEpc = HexToBin(hexEPC);
            string header;
            string itemIdentifier;

            // If EPC begins with "00", header field is 8 bits, else 2 bits
            // Ref: EPCTagDataSpecification11rev124 Page 16
            int headerLength;
            if (binaryEpc.Substring(0, 2).Equals("00"))
            {
                headerLength = 8;
            }
            else
            {
                headerLength = 2;
            }

            header = binaryEpc.Substring(0, headerLength);
            switch (header)
            {
                case (SGTIN96Header):
                    {
                        // Retrieve the partition value from 11-13 bits of EPC and
                        // get corresponding company prefix length
                        Int64 partition = BinaryToDecimal(binaryEpc.Substring(11, 3));
                        int companyPrefixLengthBinary = GetCompanyPrefixLength(Convert.ToInt32(partition), CompanyPrefixLengthMode.Bits);
                        int companyPrefixLengthDecimal = GetCompanyPrefixLength(Convert.ToInt32(partition), CompanyPrefixLengthMode.Digits);

                        // company prefix lengh + item reference length = 44 bits -
                        // get length of item reference
                        int itemReferenceLenghtBinary = 44 - companyPrefixLengthBinary;
                        int itemReferenceLenghtDecimal = 13 - companyPrefixLengthDecimal;
                        string companyPrefix = BinaryToDecimal(binaryEpc.Substring(14, companyPrefixLengthBinary)).ToString().PadLeft(companyPrefixLengthDecimal, '0');
                        string itemReference = BinaryToDecimal(binaryEpc.Substring(14 + companyPrefixLengthBinary, itemReferenceLenghtBinary)).ToString().PadLeft(itemReferenceLenghtDecimal, '0');

                        // The first digit of item reference moves to the first position of EPC
                        itemIdentifier = itemReference.Substring(0, 1) + companyPrefix + itemReference.Substring(1, itemReference.Length - 1);
                        string checkDigit = CalculateCheckDigit(itemIdentifier).ToString(); ;
                        return itemIdentifier + checkDigit;
                    }
                case (SGTIN64Header):
                    {
                        // Retrieve the company prefix index from bits 5-19
                        string companyPrefixIndex = binaryEpc.Substring(5, 14);
                        string decimalCompanyPrefixIndex = BinaryToDecimal(companyPrefixIndex).ToString();
                        string companyPrefix = GetCompanyPrefix(decimalCompanyPrefixIndex);
                        int itemReferenceLength = 13 - companyPrefix.Length;
                        string itemReference = binaryEpc.Substring(19, 20);
                        string decimalItemReference = BinaryToDecimal(itemReference).ToString().PadLeft(itemReferenceLength, '0'); ;
                        string checkDigitCalculator = decimalItemReference.Substring(0, 1) +
                            companyPrefix + decimalItemReference.Substring(1, itemReferenceLength - 1);

                        string checkDigit = CalculateCheckDigit(checkDigitCalculator).ToString(); ;
                        return checkDigitCalculator + checkDigit;
                    }
                case (SSCC96Header):
                    errorMessage = "SSCC encoded tags cannot be decoded to UPC";
                    return null;
                default:
                    errorMessage = "Invalid EPC Header. Currently supported header types are 64 and 96 bit SSCC and SGTIN";
                    return null;
            }
        }
        #endregion

        #region Private Methods
        #region HexToBin
        /// <summary>
        /// Convert a hexadecimal number to Binary
        /// </summary>
        /// <param name="hexNumber">Hexadecimal number</param>
        /// <returns>Binary representation as string</returns>
        private static string HexToBin(string hexNumber)
        {
            string inputNumber;
            string binaryNumber = "";

            //Pad input number with leading zeros to make number of digits as
            // multiple of 4
            if (hexNumber.Length % 4 != 0)
            {
                int paddedSize = Convert.ToInt32(hexNumber.Length / 4) * 4 + 4;
                inputNumber = hexNumber.PadLeft(paddedSize, '0');
            }
            else
            {
                inputNumber = hexNumber;
            }

            // Convert each digit of hexadecimal number to
            // equivalent binary
            foreach (char hexDigit in inputNumber)
            {
                string binaryBlock;
                switch (hexDigit)
                {
                    case '0':
                        binaryBlock = "0000";
                        break;
                    case '1':
                        binaryBlock = "0001";
                        break;
                    case '2':
                        binaryBlock = "0010";
                        break;
                    case '3':
                        binaryBlock = "0011";
                        break;
                    case '4':
                        binaryBlock = "0100";
                        break;
                    case '5':
                        binaryBlock = "0101";
                        break;
                    case '6':
                        binaryBlock = "0110";
                        break;
                    case '7':
                        binaryBlock = "0111";
                        break;
                    case '8':
                        binaryBlock = "1000";
                        break;
                    case '9':
                        binaryBlock = "1001";
                        break;
                    case 'A':
                        binaryBlock = "1010";
                        break;
                    case 'B':
                        binaryBlock = "1011";
                        break;
                    case 'C':
                        binaryBlock = "1100";
                        break;
                    case 'D':
                        binaryBlock = "1101";
                        break;
                    case 'E':
                        binaryBlock = "1110";
                        break;
                    case 'F':
                        binaryBlock = "1111";
                        break;
                    default:
                        string errorMessage = string.Format(INV_HEX_DIGIT, hexDigit);
                        throw new ApplicationException(errorMessage);
                }
                binaryNumber += binaryBlock;
            }

            return binaryNumber;
        }
        #endregion


        #region BinaryToDecimal
        /// <summary>
        /// Converts binary number to decimal
        /// </summary>
        /// <param name="binaryNumber">Number in binary format</param>
        /// <returns>Decimal number</returns>
        private static Int64 BinaryToDecimal(string binaryNumber)
        {
            Int64 decimalNumber = 0;
            int lastIndex = binaryNumber.Length - 1;
            // Starting from the last digit, convert each character into
            // 1 or 0 and use following logic -
            // For a number N5 N4 N3 N2 N1 N0 (eg. 10110)
            // 2^5*N5 + 2^4*N4 + 2^3*N3 + 2^2N2 + 2^1*N1 + 2^0*N0
            for (int index = lastIndex; index >= 0; index--)
            {
                int binaryDigit = Convert.ToInt16(binaryNumber.Substring(index, 1));
                decimalNumber += Convert.ToInt64(Math.Pow(2, Convert.ToDouble(lastIndex - index)) * binaryDigit);
            }

            return decimalNumber;
        }
        #endregion

        #region DecimalToBinary
        /// <summary>
        /// Converts decimal to binary number
        /// </summary>
        public static string DecimalToBinary(string data)
        {
            string result = string.Empty;
            int rem = 0;
            try
            {
                if (!IsNumeric(data))
                    throw new ApplicationException("Invalid Value - This is not a numeric value"); 
                else
                {
                    int num = int.Parse(data);
                    while (num > 0)
                    {
                        rem = num % 2;
                        num = num / 2;
                        result = rem.ToString() + result;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message); ;
            }
            return result;
        }

        private static bool IsNumeric(string number)
        {
            bool result = false;
            try
            {
                int temp = int.Parse(number);
                result = true;
            }
            catch (Exception)
            {
                //Do nothing.
            }
            return result;
        }
        #endregion

        #region DecimalToHex

        /// <summary>
        /// Converts decimal to hex number
        /// </summary>
        public static string DecimalToHex(string decString)
        {
            Int64 decValue = Int64.Parse(decString);
            return string.Format("{0:x}", decValue).ToUpper();
        }

        #endregion

        #region GetCompanyPrefixLength
        /// <summary>
        /// Returns the length of company prefix (binary) based on partition value
        /// </summary>
        /// <param name="partitionValue">Partition Value from EPC number</param>
        /// <param name="mode">Indicator for Binary or Decimal length</param>
        /// <returns></returns>
        private static int GetCompanyPrefixLength(int partitionValue, CompanyPrefixLengthMode mode)
        {
            switch (mode)
            {
                case CompanyPrefixLengthMode.Bits:
                    switch (partitionValue)
                    {
                        case 0:
                            return 40;
                        case 1:
                            return 37;
                        case 2:
                            return 34;
                        case 3:
                            return 30;
                        case 4:
                            return 27;
                        case 5:
                            return 24;
                        case 6:
                            return 20;
                        default:
                            string errorMessage = string.Format(INV_EPC_PARTITION, partitionValue);
                            throw new ApplicationException(errorMessage);
                    }
                case CompanyPrefixLengthMode.Digits:
                    switch (partitionValue)
                    {
                        case 0:
                            return 12;
                        case 1:
                            return 11;
                        case 2:
                            return 10;
                        case 3:
                            return 9;
                        case 4:
                            return 8;
                        case 5:
                            return 7;
                        case 6:
                            return 6;
                        default:
                            string errorMessage = string.Format(INV_EPC_PARTITION, partitionValue);
                            throw new ApplicationException(errorMessage);
                    }
                default:
                    throw new ApplicationException(INV_EPC_LENGTHMODE);
            }
        }
        #endregion


        #region CalculateCheckDigit
        /// <summary>
        /// Calculate check digit for UPC
        /// </summary>
        /// <param name="UPC"></param>
        /// <returns></returns>
        public int CalculateCheckDigit(string UPC)
        {
            int checkDigit;
            int factor = 3;
            int sum = 0;
            for (int index = UPC.Length; index > 0; index--)
            {
                sum = sum + Convert.ToInt32(UPC.Substring(index - 1, 1)) * factor;
                factor = 4 - factor;
            }
            checkDigit = ((1000 - sum) % 10);

            return checkDigit;
        }
        #endregion

        #region GetCompanyPrefix
        public static string GetCompanyPrefix(string companyprefixIndex)
        {
            //SqlConnection connection = null;
            //string errorMessage;
            //string connectionString = ConfigurationSettings.AppSettings["EPCConnectionString"];

            //try
            //{
            //    connection = new SqlConnection(connectionString);
            //    connection.Open();
            //}
            //catch
            //{
            //    errorMessage = Constants.DB_NO_CONNECTION;
            //    throw new ApplicationException(errorMessage);
            //}

            //object result;
            //try
            //{
            //    result = SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, "getCompanyPrefix", new SqlParameter("@CompanyPrefixIndex", companyprefixIndex));
            //}
            //catch (Exception exp)
            //{
            //    throw exp;
            //}
            //finally
            //{
            //    if (connection != null)
            //        connection.Dispose();
            //}


            //if (result == null)
            //{
            //    errorMessage = string.Format(Constants.DB_PRODUCT_NOT_FOUND, "Company Prefix Index", companyprefixIndex);
            //    throw new ApplicationException(errorMessage);
            //}

            //return result.ToString();
            return "";
        }
        #endregion
        #endregion

        #region Enumerators
        /// <summary>
        /// Barcode types
        /// </summary>
        public enum BarcodeType
        {
            UCC12 = 1,
            EAN13 = 2,
            GTIN = 3
        }

        /// <summary>
        /// EPC encoding types
        /// </summary>
        public enum EPCEncoding
        {
            SGTIN96 = 48,
            SSCC96 = 49,
            SGTIN64 = 2,
            SSCC64 = 8
        }

        /// <summary>
        /// Type of merchandise - This is the filter value field in EPC
        /// and is used to identify the type of merchandise
        /// </summary>
        public enum MerchandiseType
        {
            Product = 1,
            Item = 2,
            Case = 3,
            Pallet = 4
        }

        /// <summary>
        /// Has two values - Bits and Digits denoting length of company prefix in binary
        /// or decimal mode
        /// </summary>
        public enum CompanyPrefixLengthMode
        {
            Bits = 1,
            Digits = 2
        }
        #endregion

        public string CalcularDigitoControl(string formato, string codigo)
        {
            int digitoControl = 0;
            string retorno = "";
            if (formato == "EAN14")
            {
                //Se verifica que la longitud sea 13 (A este método el codigo debe llegar sin el digito de control)
                if (codigo.Length == 13)
                {
                    //Recorre los 13 (sin tener en cuenta digito de control) digitos y verifica digito de control
                    int suma = 0;
                    int num = 0;
                    for (int i = 0; i < codigo.Length; i++)
                    {
                        num = Convert.ToInt32(codigo.Substring(i, 1));

                        //Se van haciendo las multiplicaciones y la suma                    
                        if (i == 0)
                            suma = num * 3;
                        else
                        {
                            if (i % 2 == 0)
                                suma = suma + (num * 3);
                            else
                                suma = suma + num;
                        }
                    }

                    //Se obtiene la siguiente decena de la suma obtenida (Ej: Si la suma es 79, la sgte decena será 80)
                    int decena = 0;
                    while (decena < suma)
                    {
                        decena += 10;
                    }

                    //Digito de control
                    digitoControl = decena - suma;
                    retorno = digitoControl.ToString();
                }
            }
            else if (formato == "EAN13")
            {
                //Se verifica que la longitud sea 12 (A este método el codigo debe llegar sin el digito de control)
                if (codigo.Length == 12)
                {
                    //Recorre los 13 (sin tener en cuenta digito de control) digitos y verifica digito de control
                    int suma = 0;
                    int num = 0;
                    for (int i = 0; i < codigo.Length; i++)
                    {
                        num = Convert.ToInt32(codigo.Substring(i, 1));

                        //Se van haciendo las multiplicaciones y la suma                    
                        if (i == 0)
                            suma = num;
                        else
                        {
                            if (i % 2 == 0)
                                suma = suma + num;
                            else
                                suma = suma + (num * 3);
                        }
                    }

                    //Se obtiene la siguiente decena de la suma obtenida (Ej: Si la suma es 79, la sgte decena será 80)
                    int decena = 0;
                    while (decena < suma)
                    {
                        decena += 10;
                    }

                    //Digito de control
                    digitoControl = decena - suma;
                    retorno = digitoControl.ToString();
                }
            }

            return retorno;
        }
    }
}
