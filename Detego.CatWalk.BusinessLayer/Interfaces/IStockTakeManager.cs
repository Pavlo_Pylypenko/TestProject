﻿using System;
using System.Collections.Generic;
using Detego.CatWalk.BusinessLayer.Models;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Interfaces
{
    public interface IStockTakeManager
    {
        bool InsertStockTake(string site, string location, List<string> data, string readPoint, string email, int timeZone, DateTime? timeStamp);
        List<StockTake> GetStockTakes(int tenantId);
        List<StockTakesByDate> GetStockTakesByDate(int siteId);
        int CombineStockTakes(List<int> stockTakes);
        void EndStockTake(string site);
    }
}
