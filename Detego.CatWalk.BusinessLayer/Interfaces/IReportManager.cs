﻿using System;
using Detego.CatWalk.BusinessLayer.Models;

namespace Detego.CatWalk.BusinessLayer.Interfaces
{
    public interface IReportManager
    {
        StockTakeReportData GetReport(int id);
        StockTakeReportData GetReport(int siteId, DateTime date);
    }
}
