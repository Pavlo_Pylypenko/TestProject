﻿using System.Collections.Generic;
using System.IO;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Interfaces
{
    public interface IMasterDataManager
    {
        bool ImportMasterData(int tenantId, Stream fileStream, string sku, int timeZone);
        Article GetFirstArticleForTenant(int tenantId);
        TenantOperation IsMasterDataImporting(int tenantId);
        void ImportImages(int tenantId, string zipFilePath);
        List<string> GetColumns(int tenantId);
        List<string> GetMasterDataBySite(string gln);
        List<string> GetCompanyPrefixBySite(string gln);
    }
}
