﻿using System.Collections.Generic;
using Detego.CatWalk.BusinessLayer.Models;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Interfaces
{
    public interface ITenantManager
    {
        Site GetSite(string gln);
        List<string> GetSiteByUser(string userEmail);
        TenantUnit[] GetRootTenants();
        TenantUnit[] GetChildTenants(int parentId);
        TenantUnit[] GetChildTenants(int parentId, int userTenantId);
        int AddTenant(TenantUnit tenant);
        int EditTenant(TenantUnit tenant);
        bool DeleteTenant(int id);
        TenantUnit GetRootTenant(int? id);
        TenantUnit[] GetTenants(int? tenantId, int tenantUnitType);
        TenantUnit GetUserLabel(User user);
    }
}