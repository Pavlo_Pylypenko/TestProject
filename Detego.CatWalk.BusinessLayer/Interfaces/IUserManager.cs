﻿using System.Collections.Generic;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Interfaces
{
    public interface IUserManager
    {
        int AddUser(User user);
        bool EditUser(User user);
        bool CheckUserEmailChanged(User user);
        User GetUser(int userId);
        IEnumerable<User> GetUserList(int id, int? tenantId);
        bool EnableDisableUser(int userId);
        List<Role> GetRoles();
        void ChangePassword(string email, string newPassword);
    }
}
