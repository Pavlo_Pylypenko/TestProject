﻿using System;

namespace Detego.CatWalk.BusinessLayer.Interfaces
{
    public interface ILogger
    {
        void Debug(string message);
        void Trace(string message);
        void Info(string message);
        void Warn(string message);
        void Error(string message);
        void ErrorException(string message, Exception exception);
        void Fatal(string message);
        void Fatal(string message, Exception exception);
    }
}
