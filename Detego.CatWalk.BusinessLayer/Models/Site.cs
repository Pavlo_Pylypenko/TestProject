﻿using System.Collections.Generic;

namespace Detego.CatWalk.BusinessLayer.Models
{
    public class Site
    {
        public string Gln { get; set; }

        public List<string> Locations { get; set; }
    }
}
