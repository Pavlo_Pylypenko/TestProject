﻿using System;

namespace Detego.CatWalk.BusinessLayer.Models
{
    public class StockTakesByDate
    {
        public DateTime Date { get; set; }
        public int StockTakes { get; set; }
        public int Quantity { get; set; }
    }
}
