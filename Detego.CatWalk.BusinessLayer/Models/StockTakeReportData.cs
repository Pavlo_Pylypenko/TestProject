﻿using System.Collections.Generic;

namespace Detego.CatWalk.BusinessLayer.Models
{
    public class StockTakeReportData
    {
        public List<string> Columns { get; set; }

        public List<List<object>> Rows { get; set; }
    }
}
