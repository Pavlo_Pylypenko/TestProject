﻿using System;
using System.IO;
using System.Text;
using Microsoft.Azure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types

namespace Detego.CatWalk.BusinessLayer.Managers
{
    class BlobStorageManager
    {
        private static CloudStorageAccount _storageAccount;

        private static CloudBlobClient _blobClient;

        private static CloudBlobContainer _container;

        static BlobStorageManager()
        {
            // Retrieve storage account from connection string.
            _storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("PrimaryStorageConnectionString"));

            // Create the blob client.
            _blobClient = _storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a container.
            _container = _blobClient.GetContainerReference("images");

            // Create the container if it doesn't already exist.
            _container.CreateIfNotExists();

            //By default, the new container is private, meaning that you must specify your storage access key to download blobs from this container. 
            //If you want to make the files within the container available to everyone, you can set the container to be public using the following code:
            _container.SetPermissions(
                new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

        }

        public static string GetContainerUri()
        {
            return _container.Uri.ToString();
        }

        public static bool UploadBlob(int tenantId, string filePath)
        {
            CloudBlobDirectory blobDirectory = _container.GetDirectoryReference(tenantId.ToString());
            string gtin = Path.GetFileNameWithoutExtension(filePath);

            // Retrieve reference to a blob named as GTIN.
            CloudBlockBlob blockBlob = blobDirectory.GetBlockBlobReference(gtin);

            // Create or overwrite the GTIN blob with contents from a local file.
            try
            {
                using (var fileStream = File.OpenRead(filePath))
                {
                    blockBlob.UploadFromStream(fileStream);
                }

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public static string DownloadBlob(int tenantId, string gtin)
        {
            CloudBlobDirectory blobDirectory = _container.GetDirectoryReference(tenantId.ToString());

            // Retrieve reference to a blob named as GTIN.
            CloudBlockBlob blockBlob = blobDirectory.GetBlockBlobReference(gtin);

            string text;
            using (var memoryStream = new MemoryStream())
            {
                blockBlob.DownloadToStream(memoryStream);
                text = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
            }

            return text;
        }

        public static bool DeleteBlob(int tenantId, string gtin)
        {
            CloudBlobDirectory blobDirectory = _container.GetDirectoryReference(tenantId.ToString());

            // Retrieve reference to a blob named as GTIN.
            CloudBlockBlob blockBlob = blobDirectory.GetBlockBlobReference(gtin);

            try
            {
                // Delete the blob.
                blockBlob.Delete();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public static string ListBlobs()
        {
            StringBuilder listBlobs = new StringBuilder();
            // Loop over items within the container and output the length and URI.
            foreach (IListBlobItem item in _container.ListBlobs(null, false))
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;

                    listBlobs.AppendLine(string.Format("Block blob of length {0}: {1}", blob.Properties.Length, blob.Uri));

                }
                else if (item.GetType() == typeof(CloudPageBlob))
                {
                    CloudPageBlob pageBlob = (CloudPageBlob)item;

                    listBlobs.AppendLine(string.Format("Page blob of length {0}: {1}", pageBlob.Properties.Length, pageBlob.Uri));

                }
                else if (item.GetType() == typeof(CloudBlobDirectory))
                {
                    CloudBlobDirectory directory = (CloudBlobDirectory)item;

                    listBlobs.AppendLine(string.Format("Directory: {0}", directory.Uri));
                }
            }

            return listBlobs.ToString();
        }
    }
}
