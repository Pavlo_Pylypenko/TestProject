﻿using System;
using System.Net.Http;
using System.Text;
using System.Web.Http.ExceptionHandling;
using NLog;

namespace Detego.CatWalk.BusinessLayer.Managers
{
    public class NLogExceptionLogger : ExceptionLogger
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public override void Log(ExceptionLoggerContext context)
        {

            _logger.Log(LogLevel.Error, GetException(context.Exception), RequestToString(context.Request));
        }

        private static Exception GetException(Exception exp)
        {
            if (exp.InnerException == null)
            {
                return exp;
            }

            return GetException(exp.InnerException);
        }

        private static string RequestToString(HttpRequestMessage request)
        {
            var message = new StringBuilder();
            if (request.Method != null)
                message.Append(request.Method);

            if (request.RequestUri != null)
                message.Append(" ").Append(request.RequestUri);

            return message.ToString();
        }
    }


}