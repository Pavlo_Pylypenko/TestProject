﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Transactions;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Models;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Managers
{
    public class ReportManager : BaseManager, IReportManager
    {
        public StockTakeReportData GetReport(int id)
        {
            NLogLogger logger = new NLogLogger(null, "GetReport");
            logger.Info("before SP");
            List<StockTakeReport_Result> report = new List<StockTakeReport_Result>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
            {
                report = _context.StockTakeReport(id.ToString()).ToList();
                scope.Complete();
            }

            logger.Info("after SP");
            if (report.Count > 0)
            {
                int articleId = report[0].Id;
                List<string> columns = new List<string>();
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
                {
                    var cols = _context.Articles.Include(a => a.ParameterName).Where(a => a.Id == articleId).Select(a => a.ParameterName.List).FirstOrDefault();
                    columns = cols.Split(MASTERDATA_DELIMITER).ToList();
                    scope.Complete();
                }
                return CreateReportData(report, columns);
            }

            return null;
        }

        public StockTakeReportData GetReport(int siteId, DateTime date)
        {
            List<int> stockTakes = GetStockTakesForDate(siteId, date);
            StringBuilder ids = new StringBuilder();
            foreach (var id in stockTakes)
                ids.Append(id).Append(',');
            if (ids.Length > 0)
                ids.Remove(ids.Length - 1, 1);

            List<StockTakeReport_Result> report = new List<StockTakeReport_Result>();
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
            {
                report = _context.StockTakeReport(ids.ToString()).ToList();
                scope.Complete();
            }

            if (report.Count > 0)
            {
                int articleId = report[0].Id;
                List<string> columns = new List<string>();

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
                {
                    var cols = _context.Articles.Include(a => a.ParameterName).Where(a => a.Id == articleId).Select(a => a.ParameterName.List).FirstOrDefault();
                    columns = cols.Split(MASTERDATA_DELIMITER).ToList();
                    scope.Complete();
                }
                return CreateReportData(report, columns);
            }

            return null;
        }

        private StockTakeReportData CreateReportData(List<StockTakeReport_Result> report, List<string> columns)
        {
            int articleId = report[0].Id;
            int tenantId = 0;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
            {
                tenantId = _context.Articles.Where(a => a.Id == articleId).Select(a => a.TenantUnitId).FirstOrDefault();
                scope.Complete();
            }

            var data = new List<List<object>>();
            int n = 0;
            if (columns.Contains("Gtin", StringComparer.InvariantCultureIgnoreCase) || columns.Contains("Upc", StringComparer.InvariantCultureIgnoreCase))
                ++n;

            foreach (var row in report)
            {
                var temp = new List<object>();
                temp.Add(GetImageUrl(tenantId, row.GtinList));
                temp.Add(row.GtinList);
                temp.AddRange(row.Parameters.Split(MASTERDATA_DELIMITER));
                temp.Add(row.Quantity);
                data.Add(temp);
            }
            columns.Add("Quantity");
            var index = columns.FindIndex(c => c.Equals("Gtin", StringComparison.InvariantCultureIgnoreCase));
            bool isGtin = true;
            if (index == -1)
            {
                index = columns.FindIndex(c => c.Equals("Upc", StringComparison.InvariantCultureIgnoreCase));
                isGtin = false;
            }
            columns.RemoveAt(index);
            columns.Insert(0, "Image");
            columns.Insert(1, isGtin ? "GTIN" : "UPC");

            return new StockTakeReportData { Columns = columns, Rows = data };
        }

        private List<int> GetStockTakesForDate(int siteId, DateTime date)
        {
            var children = _context.TenantUnits.Where(t => t.Id == siteId).ToList();
            while (children.Count > 0 && children[0].TenantUnitType != (int)TenantUnitType.Location)
            {
                var ids = children.Select(t2 => t2.Id).ToList();
                children = _context.TenantUnits.Where(t => ids.Contains(t.ParentId.Value)).ToList();
            }

            var temp = children.Select(t => t.Id).ToList();
            return _context.StockTakes.Where(st => temp.Contains(st.TenantUnitId)).ToList().Where(st => st.Start.Date == date.Date).Select(st => st.Id).ToList();
        }

        private object GetImageUrl(int tenantId, string gtin)
        {
            var comma = gtin.IndexOf(',');
            if (comma > -1)
                gtin = gtin.Substring(0, comma);
            string blobContainerPath = BlobStorageManager.GetContainerUri();
            return string.Format(@"{0}/{1}/{2}", blobContainerPath, tenantId, gtin);
        }
    }
}
