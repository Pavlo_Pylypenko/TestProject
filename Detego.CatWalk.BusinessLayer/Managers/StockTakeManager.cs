﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using Detego.CatWalk.BusinessLayer.Helpers;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Models;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Managers
{
    public class StockTakeManager : BaseManager, IStockTakeManager
    {
        private EpcHelper _epcHelper = new EpcHelper();
        private static object _sync = new object();

        public bool InsertStockTake(string site, string location, List<string> epcList, string readPoint, string userEmail, int timeZone, DateTime? timeStamp)
        {
            DateTime utcTimeStamp = DateTime.UtcNow;

            if (timeStamp == null)
                timeStamp = DateTime.UtcNow.AddHours(timeZone);
            else
            {
                utcTimeStamp = timeStamp.Value.AddHours(-timeZone);
                if (utcTimeStamp > DateTime.UtcNow)
                {
                    timeStamp = DateTime.UtcNow.AddHours(timeZone);
                    utcTimeStamp = DateTime.UtcNow;
                }
                else
                {
                    if (utcTimeStamp < DateTime.UtcNow.AddDays(-1))
                    {
                        timeStamp = DateTime.UtcNow.AddDays(-1).AddHours(timeZone);
                        utcTimeStamp = DateTime.UtcNow.AddDays(-1);
                    }
                }
            }

            NLogLogger logger = new NLogLogger(null, @"StockTake\InsertStockTake Method=POST");
            TenantUnit siteTenant = _context.TenantUnits.Include(t => t.TenantUnits1).SingleOrDefault(t => t.Name == site && !t.Deleted && t.TenantUnitType == (int)TenantUnitType.Site);
            if (siteTenant == null)
            {
                //site is not found
                logger.Info(string.Format("site with gln={0} is not found ", site));
                return false;
            }

            TenantUnit locationTenant = _context.TenantUnits.SingleOrDefault(t => t.Name == location && t.ParentId == siteTenant.Id && !t.Deleted && t.TenantUnitType == (int)TenantUnitType.Location);

            if (locationTenant == null)
            {
                //site is not found
                logger.Info(string.Format("location with name={0} is not found ", location));
                return false;
            }

            List<int> locationIds = new List<int>();
            foreach (var loc in siteTenant.TenantUnits1)
            {
                locationIds.Add(loc.Id);
            }

            var stockTakeEPCItems = new List<StockTakeEPCItem>();
            epcList = epcList.Distinct().ToList();
            StockTake stockTake = null;

            // !! close all open yesterdays stocktakes for the site.
            CloseStockTakes(locationIds, timeStamp.Value.Date.AddMinutes(-1).AddHours(-timeZone));

            lock (_sync)
            {
                stockTake = GetStockTakeBySiteAndLocationAndDate(site, location, utcTimeStamp);

                if (stockTake == null)
                {
                    // !!insert empty stocktakes for all locations.
                    CreateStockTakesForAllSiteLocations(siteTenant, locationIds, userEmail, timeZone, timeStamp);
                }
            }

            stockTake = GetStockTakeBySiteAndLocationAndDate(site, location, utcTimeStamp);

            if (stockTake.First == null)
                stockTake.First = utcTimeStamp;

            stockTake.Last = stockTake.Last > utcTimeStamp ? stockTake.Last : utcTimeStamp;
            stockTake.First = stockTake.First < utcTimeStamp ? stockTake.First : utcTimeStamp;

            _context.SaveChanges();

            logger.Info(string.Format("Updating stock take for site with Id={0} and gln={1}, and location Id={2} ", siteTenant.Id, site, stockTake.TenantUnitId));
            var result = UpdateStockTake(siteTenant, locationIds, stockTake, epcList, stockTakeEPCItems, readPoint, timeStamp);

            logger.Info(String.Format("Stock Take for tenant with Id = {0} was updated", stockTake.TenantUnitId));

            if (result)
                EntityFrameworkHelper.BulkSave<StockTakeEPCItem>(stockTakeEPCItems, "StockTakeEPCItems");
            return result;
        }

        private void CreateStockTakesForAllSiteLocations(TenantUnit siteTenant, List<int> locationIds, string userEmail, int timeZone, DateTime? timeStamp)
        {
            NLogLogger logger = new NLogLogger(null, @"StockTake\InsertStockTake Method=POST");
            var stockTakes = new List<StockTake>();
            User user = _context.Users.SingleOrDefault(u => u.Email.ToLower() == userEmail.ToLower());
            byte[] buffer = Guid.NewGuid().ToByteArray();
            foreach (var id in locationIds)
            {
                StockTake st1 = new StockTake();
                st1.Start = timeStamp.Value.AddHours(-timeZone);
                
                st1.TimeZone = timeZone;
                st1.TenantUnitId = id;
                st1.Number = BitConverter.ToUInt32(buffer, 12).ToString();
                st1.Quantity = 0;
                if (user != null)
                    st1.UserId = user.Id;
                stockTakes.Add(st1);
            }

            EntityFrameworkHelper.Save<StockTake>(stockTakes);

            logger.Info(String.Format("Stock Takes for  site tenant with Id = {0} were inserted", siteTenant.Id));
        }

        private void CloseStockTakes(List<int> locationIds, DateTime dtUtC)
        {
            // update stock take where tenantunitid in locationsids and startdate < dtutc and enddate = null
            // set enddate = dtutc
            List<StockTake> stockTakes =
                _context.StockTakes.Where(
                    st => locationIds.Contains(st.TenantUnitId) && st.Start <= dtUtC && st.End == null).ToList();
            foreach (var stockTake in stockTakes)
            {
                stockTake.End = dtUtC; 
            }
            _context.SaveChanges();
        }

        private List<string> GetSiteEPCByStockTake(StockTake stockTake, List<int> locationIds)
        {
            var result = _context.StockTakeEPCItems.Join(_context.StockTakes, ste => ste.StockTakeId, st => st.Id, (ste, st) => new { StockTake = st, EPC = ste })
                    .Where(st => locationIds.Contains(st.StockTake.TenantUnitId) && st.StockTake.Start == stockTake.Start)
                    .Select(st => st.EPC.EPC).ToList();

            return result;
        }

        private bool UpdateStockTake(TenantUnit siteTenant, List<int> locationIds, StockTake st, List<string> epcList, List<StockTakeEPCItem> stockTakeEPCItems, string readPoint, DateTime? timeStamp)
        {
            NLogLogger logger = new NLogLogger(null, @"StockTake\InsertStockTake Method=POST");
            List<string> epcToday = GetSiteEPCByStockTake(st, locationIds);
            epcList = epcList.Except(epcToday).ToList();
            var gtins = epcList.ConvertAll(a => GetGtinFromEpc(a)).ToList();
            var gtinList = FindProduct(siteTenant, gtins);
            int epcCount = 0;
            foreach (var epc in epcList)
            {
                var gtin = GetGtinFromEpc(epc);
                if (gtin != null)
                {
                    if (!gtinList.ContainsKey(gtin))
                    {
                        logger.Info(string.Format("Product for site {0} and  gtin={1} is not found", siteTenant.Name, gtin));
                        continue;
                    }

                    int productId = gtinList[gtin];
                    StockTakeEPCItem stEpcItem = new StockTakeEPCItem();
                    stEpcItem.ProductId = productId;
                    stEpcItem.EPC = epc;
                    stEpcItem.StockTakeId = st.Id;
                    stockTakeEPCItems.Add(stEpcItem);
                    ++st.Quantity;
                    ++epcCount;
                }
                else
                    logger.Info(string.Format("Ignoring epc {0} because not valid sgtin96 epc.", epc));
            }
            logger.Info(string.Format("{0} EPC items is submitted ", epcCount));

            if (timeStamp != null)
                st.Last = timeStamp.Value.AddHours(-st.TimeZone) > st.Last ? timeStamp.Value.AddHours(-st.TimeZone) : st.Last;

            if (!string.IsNullOrEmpty(readPoint))
            {
                if (st.Readpoint == null)
                    st.Readpoint = string.Empty;

                var readpoints = st.Readpoint.Split(',');
                if (!readpoints.Contains(readPoint))
                    if (st.Readpoint.Length + readPoint.Length + 1 <= 64)
                        if (st.Readpoint.Length > 0)
                            st.Readpoint += string.Concat(",", readPoint);
                        else
                            st.Readpoint += readPoint; 
            }

            _context.SaveChanges();
            return true;
        }

        private StockTake GetStockTakeBySiteAndLocationAndDate(string site, string location, DateTime dateTime)
        {
            StockTake stockTake = _context.StockTakes.Where(s => s.TenantUnit.Name == location && s.TenantUnit.TenantUnit1.Name == site && ((s.End == null) || (dateTime <= s.End.Value))).OrderBy(s => s.Start).Take(1).FirstOrDefault();
            return stockTake;
        }

        private Dictionary<string, int> FindProduct(TenantUnit tenant, List<string> gtins)
        {
            var masterDataManager = new MasterDataManager();
            int? tenantId = masterDataManager.GetNearestTenantWithMasterData(tenant);

            if (tenantId != null)
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
                {
                    var gtinList = _context.Products.Where(p => p.TenantUnitId == tenantId.Value && gtins.Contains(p.Gtin)).ToDictionary(p => p.Gtin, p => p.Id);
                    scope.Complete();
                    return gtinList;
                }
            }
            return new Dictionary<string, int>();
        }

        public List<StockTake> GetStockTakes(int tenantId)
        {
            var stockTakes = _context.StockTakes.Where(st => st.TenantUnitId == tenantId).ToList();
            stockTakes.ForEach(st =>
            {
                if (st.End == null && CheckNotToday(st.Start.AddHours(st.TimeZone).Date, st.TimeZone))
                    st.End = st.Start.Date.AddDays(1).AddMinutes(-1).AddHours(-st.TimeZone);
                _context.SaveChanges();
            });

            stockTakes = stockTakes.Where(a => a.First != null || a.Last != null).ToList();

            stockTakes.ForEach(st =>
            {
                st.Start = st.Start.AddHours(st.TimeZone);
                if (st.First.HasValue)
                {
                    st.First = st.First.Value.AddHours(st.TimeZone);
                }

                if (st.Last.HasValue)
                {
                    st.Last = st.Last.Value.AddHours(st.TimeZone);
                }
                if (st.End != null)
                    st.End = st.End.Value.AddHours(st.TimeZone);
            });
            return stockTakes;
        }

        public List<StockTakesByDate> GetStockTakesByDate(int siteId)
        {
            var allStockTakes = _context.StockTakes.Where(st => st.TenantUnitId == siteId).ToList();
            var children = _context.TenantUnits.Where(t => t.Id == siteId && !t.Deleted).ToList();
            while (children.Count > 0 && children[0].TenantUnitType != (int)TenantUnitType.Location)
            {
                var ids = children.Select(t2 => t2.Id).ToList();
                children = _context.TenantUnits.Where(t => ids.Contains(t.ParentId.Value)).ToList();
            }

            var temp = children.Select(t => t.Id).ToList();
            allStockTakes.AddRange(_context.StockTakes.Where(st => temp.Contains(st.TenantUnitId)).ToList());

            if (allStockTakes.Count > 0)
            {
                var result = allStockTakes.GroupBy(st => st.Start.AddHours(st.TimeZone).Date).Select(group => new StockTakesByDate { Date = group.Key, StockTakes = group.Count(st => st.First != null || st.Last != null), Quantity = group.Sum(st => st.Quantity) }).OrderBy(s => s.Quantity).ToList();
                return result;
            }
            return new List<StockTakesByDate>();
        }

        public int CombineStockTakes(List<int> stockTakeIds)
        {
            NLogLogger logger = new NLogLogger(null, @"StockTakes\CombineStockTake Method=POST");
            List<StockTakeEPCItem> stockTakeEpcItems = new List<StockTakeEPCItem>();
            List<StockTakeEPCItem> stockTakeEpcItemsToAdd = new List<StockTakeEPCItem>();
            List<StockTakeEPCItem> stockTakeEpcItemsToDelete = new List<StockTakeEPCItem>();

            List<StockTake> stockTakes =
                _context.StockTakes.Where(st => stockTakeIds.Contains(st.Id)).OrderBy(a => a.Id).ToList();

            if (stockTakes.Count != 2)
            {
                logger.Error("Should be selected only two stocktakes to combine.");
                throw new Exception("Should be selected only two stocktakes to combine."); 
            }
                

            int tenantId = stockTakes.First().TenantUnitId;

            if (stockTakes.Any(s => s.TenantUnitId != tenantId))
            {
                logger.Error("All stocktakes should have the same tenant to be combined.");
                throw new Exception("All stocktakes should have the same tenant to be combined.");
            }

            try
            {
                StockTake combinedStockTake = null;
                combinedStockTake = stockTakes.First();
                var stockTakeToDelete = stockTakes[1];

                var epcFromCombined = _context.StockTakeEPCItems.Where(stei => stei.StockTakeId == combinedStockTake.Id)
                    .ToDictionary(a => a.EPC, a => a);

                var epcFromDeleted = _context.StockTakeEPCItems.Where(stei => stei.StockTakeId == stockTakeToDelete.Id).Select(a => a).ToList();

                foreach (StockTakeEPCItem epc in epcFromDeleted)
                {
                    if (epcFromCombined.ContainsKey(epc.EPC))
                    {
                        stockTakeEpcItemsToDelete.Add(epc);
                    }
                    else
                    {
                        stockTakeEpcItemsToAdd.Add(epc);
                    }
                }

                _context.StockTakeEPCItems.RemoveRange(stockTakeEpcItemsToDelete);

                combinedStockTake.End = stockTakeToDelete.End ?? DateTime.UtcNow;
                combinedStockTake.Quantity = epcFromCombined.Count + stockTakeEpcItemsToAdd.Count;
                combinedStockTake.Last = stockTakeToDelete.Last == null ? stockTakeToDelete.Last : combinedStockTake.Last;
                combinedStockTake.First = combinedStockTake.First == null ? stockTakeToDelete.First : combinedStockTake.First;

                var emptyToDelete = _context.StockTakes.Where(
                    a =>
                        (a.Start >= combinedStockTake.Start) && (a.First == null || a.Last == null) && (a.TenantUnitId == combinedStockTake.TenantUnitId) &&
                        (combinedStockTake.End == null || (a.End < combinedStockTake.End)));

                _context.StockTakes.RemoveRange(emptyToDelete);

                _context.SaveChanges(); 

                //stockTakeEpcItemsToDelete = new List<StockTakeEPCItem>();
                //add new items to combinedStockTake
                foreach (StockTakeEPCItem stockTakeEpcItem in stockTakeEpcItemsToAdd)
                {
                    StockTakeEPCItem newItem = new StockTakeEPCItem();
                    newItem.StockTakeId = combinedStockTake.Id;
                    newItem.EPC = stockTakeEpcItem.EPC;
                    newItem.ProductId = stockTakeEpcItem.ProductId;
                    stockTakeEpcItems.Add(newItem);
                }

                _context.StockTakeEPCItems.RemoveRange(stockTakeEpcItemsToAdd);
                _context.StockTakes.Remove(stockTakeToDelete);
                _context.SaveChanges();

                EntityFrameworkHelper.BulkSave<StockTakeEPCItem>(stockTakeEpcItems, "StockTakeEPCItems");
                return combinedStockTake.Id;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public void EndStockTake(string site)
        {
            NLogLogger logger = new NLogLogger(null, @"StockTake\EndStockTake ");
            TenantUnit siteTenant = _context.TenantUnits.Include(t => t.TenantUnits1).SingleOrDefault(t => t.Name == site && !t.Deleted && t.TenantUnitType == (int)TenantUnitType.Site);
            if (siteTenant == null)
            {
                //site is not found
                logger.Info(string.Format("site with gln={0} is not found ", site));
            }
            List<int> locationIds = new List<int>();

            foreach (var loc in siteTenant.TenantUnits1)
            {
                locationIds.Add(loc.Id);
            }

            CloseStockTakes(locationIds, DateTime.UtcNow);
        }

        private string GetGtinFromEpc(string epc)
        {
            return _epcHelper.GetEAN13(epc);
        }

        private bool CheckNotToday(DateTime date, int timeZone)
        {
            var currentDate = DateTime.UtcNow.AddHours(timeZone);

            return (date.Day < currentDate.Day);
        }
    }
}
