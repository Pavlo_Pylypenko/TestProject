﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using Detego.CatWalk.BusinessLayer.Helpers;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Managers
{
    public class UserManager : BaseManager, IUserManager
    {
        public UserManager()
        {
            if (!_context.Users.Any())
            {
                InsertDefaultUserAndRole();
            }
        }

        private void InsertDefaultUserAndRole()
        {
            var rootRole = new Role {Name = RoleHelper.Root};
            _context.Roles.Add(rootRole);
            var roles = new List<Role> {new Role {Name = RoleHelper.Admin}, new Role {Name = RoleHelper.RegionalManager}, new Role {Name = RoleHelper.StoreManager}, new Role {Name = RoleHelper.User}};
            _context.Roles.AddRange(roles);
            _context.SaveChanges();

            var tenant = new TenantUnit {Name = "Label 1", TenantUnitType = (int) TenantUnitType.Label};
            _context.TenantUnits.Add(tenant);
            _context.SaveChanges();

            var user = new User() {UserName = "Root", Password = PasswordHelper.CalculateMD5Hash("root1234"), RoleId = rootRole.Id, DateCreated = DateTime.Now.Date, Email = "root@mail.com", TenantUnitId = null};      
            _context.Users.Add(user);
            _context.SaveChanges();

            var tenantManager = new TenantManager();
            tenantManager.CreateUserForTenant(tenant);
        }

        public int AddUser(User user)
        {
            if (user.Id == 0)
            {
                user.Role = null;
                user.TenantUnit = null;
                using (var context = new DetegoCatWalkDbContext())
                {
                    context.Users.Add(user);

                    try
                    {
                        context.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        NLogLogger logger = new NLogLogger(null, "AddUser");

                        foreach (var eve in e.EntityValidationErrors)
                        {
                            logger.Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                            foreach (var ve in eve.ValidationErrors)
                            {
                                logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                            }
                        }
                    }
                }
            }
            else
            {
                return -1;
            }

            return user.Id;
        }

        public bool CheckUserEmailChanged(User user)
        {
            using (var context = new DetegoCatWalkDbContext())
            {
                User dbEntry = context.Users.FirstOrDefault(u => u.Id == user.Id);
                if (dbEntry != null)
                {
                    return dbEntry.Email != user.Email;
                }
            }

            return false;
        }

        public bool EditUser(User user)
        {
            try
            {
                using (var context = new DetegoCatWalkDbContext())
                {
                    User dbEntry = context.Users.FirstOrDefault(u => u.Id == user.Id);
                    if (dbEntry != null)
                    {
                        dbEntry.DateCreated = user.DateCreated;
                        dbEntry.Deactivated = user.Deactivated;
                        dbEntry.Email = user.Email;
                        dbEntry.RoleId = user.RoleId;
                        dbEntry.TenantUnitId = user.TenantUnitId;
                        dbEntry.UserName = user.UserName;
                        if (!string.IsNullOrEmpty(user.Password) && (user.Password != dbEntry.Password))
                        {
                            dbEntry.Password = user.Password;
                        }
                        dbEntry.Fax = user.Fax;
                        dbEntry.Phone = user.Phone;
                        dbEntry.EmployeeId = user.EmployeeId;
                        dbEntry.LastSignIn = user.LastSignIn;
                        context.SaveChanges();
                        return true;
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                NLogLogger logger = new NLogLogger(null, "EditUser");

                foreach (var eve in e.EntityValidationErrors)
                {
                    logger.Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }

            return false;
        }

        public User GetUser(int userId)
        {
            var user = _context.Users.Include(u => u.Role).Include(u => u.TenantUnit.TenantUnit1).SingleOrDefault(u => u.Id == userId && ((u.TenantUnit != null && !u.TenantUnit.Deleted) || u.TenantUnit == null));
            if (user.TenantUnitId == null)
                return new User { DateCreated = user.DateCreated, Deactivated = user.Deactivated, Email = user.Email, EmployeeId = user.EmployeeId, Fax = user.Fax, Id = user.Id, LastSignIn = user.LastSignIn, Phone = user.Phone, Role = new Role { Id = user.Role.Id, Name = user.Role.Name }, TenantUnitId = null, UserName = user.UserName, FailedLogins = user.FailedLogins, Password = user.Password, RoleId = user.RoleId};
            return new User {DateCreated = user.DateCreated, Deactivated = user.Deactivated, Email = user.Email, EmployeeId = user.EmployeeId, Fax = user.Fax, Id = user.Id, LastSignIn = user.LastSignIn, Phone = user.Phone, Role = new Role {Id = user.Role.Id, Name = user.Role.Name}, TenantUnit = new TenantUnit {Id = user.TenantUnit.Id, Name = user.TenantUnit.Name}, UserName = user.UserName, TenantUnitId = user.TenantUnitId, Password = user.Password, RoleId = user.RoleId, FailedLogins = user.FailedLogins };
        }

        public IEnumerable<User> GetUserList(int id, int? tenantId)
        {
            var users = new List<User>();
           
            using (var context = new DetegoCatWalkDbContext())
            {
                if (tenantId == null)
                    return context.Users.Include(u => u.Role).Include(u => u.TenantUnit).Where(u => u.Role.Name != RoleHelper.Root && ((u.TenantUnit != null && !u.TenantUnit.Deleted) || u.TenantUnit == null)).ToList();

                var tenant = context.TenantUnits.Include(t => t.Users.Select(u => u.Role)).FirstOrDefault(tu => tu.Id == tenantId && !tu.Deleted);
                var currentUser = tenant.Users.First(u => u.Id == id);
                users.Add(new User { DateCreated = currentUser.DateCreated, Deactivated = currentUser.Deactivated, Email = currentUser.Email, EmployeeId = currentUser.EmployeeId, Fax = currentUser.Fax, Id = currentUser.Id, LastSignIn = currentUser.LastSignIn, Phone = currentUser.Phone, Role = new Role { Id = currentUser.Role.Id, Name = currentUser.Role.Name }, TenantUnit = new TenantUnit { Id = currentUser.TenantUnit.Id, Name = currentUser.TenantUnit.Name }, UserName = currentUser.UserName });

                if (tenant.TenantUnitType == 3)
                    foreach (var user in tenant.Users)
                        if (user.Role.Name == RoleHelper.User)
                            users.Add(new User { DateCreated = user.DateCreated, Deactivated = user.Deactivated, Email = user.Email, EmployeeId = user.EmployeeId, Fax = user.Fax, Id = user.Id, LastSignIn = user.LastSignIn, Phone = user.Phone, Role = new Role { Id = user.Role.Id, Name = user.Role.Name }, TenantUnit = new TenantUnit { Id = user.TenantUnit.Id, Name = currentUser.TenantUnit.Name }, UserName = user.UserName });

                var children = context.TenantUnits.Include(t => t.Users.Select(u => u.Role)).Where(t => t.ParentId == tenantId && !t.Deleted).ToList();
                do
                {
                    foreach (var child in children)
                        foreach (var user in child.Users.ToList())
                            if (RoleHelper.IsLessRole(currentUser.Role.Name, user.Role.Name))
                                users.Add(new User {DateCreated = user.DateCreated, Deactivated = user.Deactivated, Email = user.Email, EmployeeId = user.EmployeeId, Fax = user.Fax, Id = user.Id, LastSignIn = user.LastSignIn, Phone = user.Phone, Role = new Role {Id = user.Role.Id, Name = user.Role.Name}, TenantUnit = new TenantUnit {Id = user.TenantUnit.Id, Name = user.TenantUnit.Name}, UserName = user.UserName});
                    var ids = children.Select(t => t.Id).ToList();
                    children = context.TenantUnits.Include(t => t.Users.Select(u => u.Role)).Where(t => ids.Contains(t.ParentId.Value) && !t.Deleted).ToList();
                } while (children.Count > 0);
            }

            return users;
        }

        public bool EnableDisableUser(int userId)
        {
            try
            {
                using (var context = new DetegoCatWalkDbContext())
                {
                    User dbEntry = context.Users.FirstOrDefault(u => u.Id == userId);
                    if (dbEntry != null)
                    {
                        dbEntry.Deactivated = !dbEntry.Deactivated;
                        context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Role> GetRoles()
        {
            return _context.Roles.Where(r => r.Name != RoleHelper.Root).ToList();
        }

        public void ChangePassword(string email, string newPassword)
        {
            var user = _context.Users.FirstOrDefault(u => u.Email == email);
            if (user != null)
            {
                user.Password = PasswordHelper.CalculateMD5Hash(newPassword);
                _context.SaveChanges();
            }
        }
    }
}
