﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Transactions;
using Detego.CatWalk.BusinessLayer.Helpers;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Managers
{
    public class MasterDataManager : BaseManager, IMasterDataManager
    {
        private int _companyPrefixLength = 7;

        public List<string> GetMasterDataBySite(string gln)
        {
            TenantUnit tenant = _context.TenantUnits.SingleOrDefault(tu => tu.Name == gln && tu.TenantUnitType == (int)TenantUnitType.Site && !tu.Deleted);
            if (tenant != null)
            {
                return GetGtinListByTenant(tenant);
            }
            return new List<string>();
        }

        public List<string> GetCompanyPrefixBySite(string gln)
        {
            List<string> gtinList = new List<string>();
            TenantUnit tenant = _context.TenantUnits.SingleOrDefault(tu => tu.Name == gln && tu.TenantUnitType == (int)TenantUnitType.Site  && !tu.Deleted);
            if (tenant != null)
            {
                gtinList = GetCompanyPrefixesByTenant(tenant);
            }

            return gtinList;
        }

        public Article GetFirstArticleForTenant(int tenantId)
        {
            Article result;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
            {
                result = _context.Articles.Include(a => a.ParameterName).FirstOrDefault(a => a.TenantUnitId == tenantId);
                scope.Complete();
            }
            return result;
        }

        public void ImportImages(int tenantId, string zipFilePath)
        {   
            NLogLogger logger = new NLogLogger(null, @"MasterData\ImportImages Method=POST");
            List<string> gtinList = new List<string>();
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
            {
                gtinList = _context.Products.Where(p => p.TenantUnitId == tenantId).Select(g => g.Gtin).ToList();
                scope.Complete();
            }
            string extractPath = Path.Combine(Path.GetDirectoryName(zipFilePath), "Extract");
            try
            {
                System.IO.Compression.ZipFile.ExtractToDirectory(zipFilePath, extractPath);
                DirectoryInfo di = new DirectoryInfo(extractPath);
                int imagesImported = 0;

                foreach (FileInfo file in di.GetFiles())
                {

                    string fileExrension = file.Extension.ToLower();
                    if (fileExrension != ".jpg" && fileExrension != ".jpeg" && fileExrension != ".png")
                    {
                        logger.Info(string.Format("Ignoring image {0} because not valid image extension. If should be jpg, jpeg or png.", file.Name));
                        continue;
                    }

                    if (!gtinList.Contains(Path.GetFileNameWithoutExtension(file.Name)))
                    {
                        logger.Info(string.Format("Master data for Tenant with Id = {0} doesn't contain gtin = {1}.", tenantId, Path.GetFileNameWithoutExtension(file.Name)));
                        continue;
                    }

                    Image img = Image.FromFile(file.FullName);
                    if ((img.Width != 64) && (img.Height != 64))
                    {
                        logger.Info(string.Format("Ignoring image {0} because not valid image size. It should be 64 pixels for height and width.", file.Name));
                        continue;
                    }

                    BlobStorageManager.UploadBlob(tenantId, file.FullName);
                    ++imagesImported;
                }
                logger.Info(imagesImported + " images were imported");
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Images wasn't imported. Reason: {0}", NLogLogger.GetException(ex).Message));
            }
            finally
            {
                File.Delete(zipFilePath);
                Directory.Delete(extractPath, true);
            }
        }

        public List<string> GetColumns(int tenantId)
        {
            var article = GetFirstArticleForTenant(tenantId);
            if (article != null)
            {
                List<string> columns = _context.ParameterNames.First(p => p.Id == article.ParameterNamesId).List.Split(MASTERDATA_DELIMITER).ToList();
                return columns;
            }
            return null;
        }

        public bool ImportMasterData(int tenantId, Stream fileStream, string sku, int timeZone)
        {
            TimeSpan timeout = new TimeSpan(1, 0, 0);
            OverrideTransactionScopeMaximumTimeout(timeout);
            bool newOperation = false;

            var tenantOperation = _context.TenantOperations.FirstOrDefault(t => t.TenantUnitId == tenantId && t.Operation == (int)TenantOperationType.ImportMasterData);
            if (tenantOperation != null)
            {
                if (tenantOperation.OperationStatus == (int) OperationStatus.Started)
                    return false;
                else
                {
                    tenantOperation.OperationStatus = (int) OperationStatus.Started;
                    tenantOperation.DateCompleted = DateTime.UtcNow.AddHours(timeZone);
                }  
            }
            else
            {
                newOperation = true;
                tenantOperation = new TenantOperation() {Operation = (int) TenantOperationType.ImportMasterData, OperationStatus = (int) OperationStatus.Started, TenantUnitId = tenantId, DateCompleted = DateTime.UtcNow.AddHours(timeZone)};
                _context.TenantOperations.Add(tenantOperation);
            }
       
            try
            {
                _context.SaveChanges();

                int recordsImported;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { Timeout = timeout, IsolationLevel = IsolationLevel.ReadUncommitted }))
                {
                    recordsImported = ProcessCsv(tenantId, fileStream, sku, MASTERDATA_DELIMITER);
                    scope.Complete();
                }
                tenantOperation.OperationStatus = (int) OperationStatus.Completed;
                tenantOperation.Count = recordsImported;
                tenantOperation.DateCompleted = DateTime.UtcNow.AddHours(timeZone);
                _context.SaveChanges(); 
            }
            catch (Exception ex)
            {
                NLogLogger logger = new NLogLogger(null, @"MasterData\ImportMasterData Method=POST");
                logger.Error(string.Format("Master Data wasn't imported. Reason: {0}", NLogLogger.GetException(ex).Message));
                if (!newOperation)
                    tenantOperation.OperationStatus = (int)OperationStatus.Completed;
                else
                    _context.TenantOperations.Remove(tenantOperation);
                _context.SaveChanges(); 
            }

            return true;
        }

        private int UpdateMasterData(Article first, StreamReader reader, string sku, char delimiter, List<string> columnNames)
        {
            var articlesInsert = new Dictionary<string, Article>();
            var productsInsert = new List<Product>();

            string[] skuFields = sku.Split(delimiter);
            string line;
            var fromCsv = new Dictionary<string, CsvValues>();

            while ((line = reader.ReadLine()) != null)
            {
                string[] columnValues = line.Split(delimiter);
                var skuValues = new List<string>();
                int j = 0;

                foreach (var columnName in columnNames)
                {
                    if (skuFields.Contains(columnName))
                    {
                        skuValues.Add(columnValues[j]);
                    }
                    ++j;
                }

                string skuValuesString = string.Join(delimiter.ToString(), skuValues);
                if (fromCsv.ContainsKey(skuValuesString))
                    fromCsv[skuValuesString].Parameters.Add(columnValues);
                else
                    fromCsv.Add(skuValuesString, new CsvValues{ Parameters = new List<string[]> {columnValues}, IsNew = true});
            }

            int step = 0;
            Dictionary<string, string> allSku = new Dictionary<string, string>();
            do
            {
                allSku = _context.Articles.Where(a => a.TenantUnitId == first.TenantUnitId).OrderBy(a => a.Id).Select(a => a.Sku).Skip(100000 * step).Take(100000).ToDictionary(s => s);
                ++step;
                foreach (var keyvalue in fromCsv)
                {
                    if (allSku.ContainsKey(keyvalue.Key))
                        keyvalue.Value.IsNew = false;
                }
            } 
            while (allSku.Keys.Count > 0);

                            var sku10000 = new List<string>();
                int inserted = 0;
            foreach(var keyvalue in fromCsv)
            {
                if (keyvalue.Value.IsNew)
                {
                    Article article = new Article();
                    article.ParameterNamesId = first.ParameterNamesId;
                    article.Sku = keyvalue.Key;
                    article.TenantUnitId = first.TenantUnitId;
                    article.Parameters = string.Empty;
                    article.GtinList = string.Empty;
                    article.Products = new List<Product>();
                    articlesInsert.Add(keyvalue.Key, article);
                    ComposeNewArticle(articlesInsert[keyvalue.Key], columnNames, keyvalue.Value.Parameters, productsInsert, delimiter);

                    if (articlesInsert.Count == 10000)
                    {
                        List<Article> articlesList = articlesInsert.Select(it => it.Value).ToList();
                        EntityFrameworkHelper.BulkSave(articlesList, "Articles");
                        List<int> articleIdList = _context.Articles.Where(a => a.TenantUnitId == first.TenantUnitId).OrderByDescending(a => a.Id).Select(a => a.Id).Take(articlesInsert.Count).ToList().OrderBy(id => id).ToList();
                        for (int i = 0; i < articlesInsert.Count; i++)
                        {
                            foreach (var product in articlesList[i].Products)
                            {
                                product.ArticleId = articleIdList[i];
                            }
                        }
                        EntityFrameworkHelper.BulkSave(productsInsert, "Products");

                        _context.SaveChanges();

                        articlesInsert.Clear();
                        productsInsert.Clear();
                        inserted += 10000;
                    }
                }
                else
                {
                    sku10000.Add(keyvalue.Key);
                    if (sku10000.Count > 10000)
                    {
                        using (var context = new DetegoCatWalkDbContext())
                        {
                            var articles1000 = context.Articles.Include(a => a.Products).Where(a => sku10000.Contains(a.Sku) && a.TenantUnitId == first.TenantUnitId).ToList();

                            foreach (var article in articles1000)
                            {
                                UpdateArticle(article, columnNames, fromCsv[article.Sku].Parameters, delimiter);
                            }
                            context.SaveChanges();
                        }
                        sku10000.Clear();
                    }
                }
            }

            if (sku10000.Count > 0)
            {
                var articlesLast = _context.Articles.Include(a => a.Products).Where(a => sku10000.Contains(a.Sku) && a.TenantUnitId == first.TenantUnitId).ToList();

                foreach (var article in articlesLast)
                {
                    UpdateArticle(article, columnNames, fromCsv[article.Sku].Parameters, delimiter);
                }
                _context.SaveChanges();
            }
            if (articlesInsert.Count > 0)
            {
                List<Article> articlesList = articlesInsert.Select(it => it.Value).ToList();
                EntityFrameworkHelper.BulkSave(articlesList, "Articles");
                List<int> articleIdList = _context.Articles.Where(a => a.TenantUnitId == first.TenantUnitId).OrderByDescending(a => a.Id).Select(a => a.Id).Take(articlesInsert.Count).ToList().OrderBy(id => id).ToList();
                for (int i = 0; i < articlesInsert.Count; i++)
                {
                    foreach (var product in articlesList[i].Products)
                    {
                        product.ArticleId = articleIdList[i];
                    }
                }
                EntityFrameworkHelper.BulkSave(productsInsert, "Products");

                _context.SaveChanges();
                inserted += articlesInsert.Count;

                articlesInsert.Clear();
                productsInsert.Clear();
            }

            return inserted;
        }

        private void UpdateArticle(Article article, List<string> columnNames, List<string[]> newValues, char delimiter)
        {
            int c = 0;
            StringBuilder paramBuilder = new StringBuilder();

            foreach (var columnName in columnNames)
            {
                switch (GetDatabaseFieldName(columnName))
                {
                    case "Gtin":
                        var gtinList = GtinStringToList(article.GtinList);
                        for (int i = 0; i < newValues.Count; i++)
                        {
                            var product = new Product();

                            if (article.Products.All(p => p.Gtin != newValues[i][c]))
                            {
                                product.ArticleId = article.Id;
                                product.Gtin = newValues[i][c];
                                product.TenantUnitId = article.TenantUnitId;
                                article.Products.Add(product);
                                gtinList.Add(newValues[i][c]);
                            }
                        }
                        article.GtinList = string.Join(",", gtinList);

                        break;
                    default:
                        paramBuilder.AppendFormat("{0}{1}", newValues[0][c], delimiter);
                        break;
                }
                ++c;
            }

            article.Parameters = paramBuilder.ToString(0, paramBuilder.Length - 1);
        }

        private void ComposeNewArticle(Article article, List<string> columnNames, List<string[]> values, List<Product> productsInsert, char delimiter)
        {
            int c = 0;
            StringBuilder paramBuilder = new StringBuilder();

            foreach (var columnName in columnNames)
            {
                switch (GetDatabaseFieldName(columnName))
                {
                    case "Gtin":
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < values.Count; i++)
                        {
                            var product = new Product();

                            if (article.Products.All(p => p.Gtin != values[i][c]))
                            {
                                product.Article = article;
                                product.Gtin = values[i][c];
                                product.TenantUnitId = article.TenantUnitId;
                                productsInsert.Add(product);
                                article.Products.Add(product);
                                stringBuilder.AppendFormat("{0}{1}", values[i][c], ',');
                            }
                        }
                        article.GtinList = stringBuilder.ToString(0, stringBuilder.Length - 1);

                        break;
                    default:
                        paramBuilder.AppendFormat("{0}{1}", values[0][c], delimiter);

                        break;
                }
                ++c;
            }

            article.Parameters = paramBuilder.ToString(0, paramBuilder.Length - 1);
        }

        private int ProcessCsv(int tenantId, Stream fileStream, string sku, char delimiter)
        {
            var articlesInsert = new Dictionary<string, Article>();
            var productsInsert = new List<Product>();
            var articlesExisting = new Dictionary<string, Article>();
            Article firstArticleForTenant = GetFirstArticleForTenant(tenantId);
            bool isMasterDataImported = firstArticleForTenant != null;
            int recordsInserted = 0;

            using (StreamReader reader = new StreamReader(fileStream))
            {
                string line = reader.ReadLine();

                var columnNames = line.Split(delimiter).ToList();
                ParameterName parameterName = null;

                if (isMasterDataImported)
                {
                    recordsInserted = UpdateMasterData(firstArticleForTenant, reader, sku, delimiter, columnNames);
                }
                else
                {
                    parameterName = CreateParameterName(line, sku);
                    string[] skuFields = sku.Split(delimiter);

                    while ((line = reader.ReadLine()) != null)
                    {
                        //Debug.WriteLine("j={0}, {1}", j, HiResDateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture));
                        string[] columnValues = line.Split(delimiter);
                        var article = new Article();

                        var skuValues = new List<string>();
                        int i = 0;

                        foreach (var columnName in columnNames)
                        {
                            if (skuFields.Contains(columnName))
                            {
                                skuValues.Add(columnValues[i]);
                            }
                            i = i + 1;
                        }

                        string skuValuesString = string.Join(delimiter.ToString(), skuValues);

                        if (!articlesInsert.ContainsKey(skuValuesString))
                        {
                            article = new Article();
                            article.ParameterNamesId = parameterName.Id;
                            article.Sku = skuValuesString;
                            article.TenantUnitId = tenantId;
                            article.Parameters = string.Empty;
                            article.GtinList = string.Empty;
                            article.Products = new List<Product>();
                            articlesInsert.Add(skuValuesString, article);
                        }
                        else
                        {
                            article = articlesInsert[skuValuesString];
                            article.Parameters = String.Empty;
                        }
                        i = 0;
                        var gtinList = GtinStringToList(article.GtinList);

                        foreach (var columnName in columnNames)
                        {
                            switch (GetDatabaseFieldName(columnName))
                            {
                                case "Gtin":
                                    var product = new Product();
                                    if (article.Id == 0)
                                    {
                                        if (article.Products.All(p => p.Gtin != columnValues[i]))
                                        {
                                            product.Article = article;
                                            product.Gtin = columnValues[i];
                                            product.TenantUnitId = article.TenantUnitId;
                                            productsInsert.Add(product);
                                            article.Products.Add(product);
                                            gtinList.Add(columnValues[i]);
                                        }
                                    }
                                    else
                                    {
                                        //Update
                                        if (articlesExisting.ContainsKey(skuValuesString))
                                        {
                                            product = articlesExisting[article.Sku].Products.FirstOrDefault(p => p.Gtin == columnValues[i]);
                                            if (product == null)
                                            {
                                                product = CreateProduct(article, columnValues, i, productsInsert);
                                                gtinList.Add(columnValues[i]);
                                            }
                                        }
                                        else
                                        {
                                            product = CreateProduct(article, columnValues, i, productsInsert);
                                            gtinList.Add(columnValues[i]);
                                        }
                                    }

                                    break;
                                default:
                                    article.Parameters = string.Format("{0}{1}{2}", article.Parameters, delimiter, columnValues[i]);
                                    break;
                            }
                            i = i + 1;
                        }
                        article.GtinList = string.Join(",", gtinList.Distinct());
                        article.Parameters = article.Parameters.TrimStart(delimiter);
                    }
                }
            }

            _context.SaveChanges();

            if (!isMasterDataImported)
            {
                List<Article> articlesList = articlesInsert.Select(it => it.Value).ToList();
                EntityFrameworkHelper.BulkSave(articlesList, "Articles");
                List<int> articleIdList = _context.Articles.Where(a => a.TenantUnitId == tenantId).OrderByDescending(a => a.Id).Select(a => a.Id).Take(articlesInsert.Count).ToList().OrderBy(id => id).ToList();
                for (int i = 0; i < articlesInsert.Count; i++)
                {
                    foreach (var product in articlesList[i].Products)
                    {
                        product.ArticleId = articleIdList[i];
                    }
                }
                EntityFrameworkHelper.BulkSave(productsInsert, "Products");

                _context.SaveChanges();
            }
            int recordsImported = articlesInsert.Keys.Count + recordsInserted;
            NLogLogger logger = new NLogLogger(null, @"MasterData\ImportMasterData Method=POST ");
            logger.Info(recordsImported + " Articles were imported");
            return recordsImported;
        }

        private static Product CreateProduct(Article article, string[] columnValues, int i, List<Product> productsInsert)
        {
            Product product = new Product();
            product.ArticleId = article.Id;
            product.Gtin = columnValues[i];
            product.TenantUnitId = article.TenantUnitId;
            productsInsert.Add(product);
            return product;
        }

        private string GetDatabaseFieldName(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "upc":
                case "gtin":
                    return "Gtin";
                default:
                    return "Parameter";
            }
        }

        private ParameterName CreateParameterName(string paramName, string sku)
        {
            ParameterName pn = new ParameterName();
            pn.List = paramName;
            pn.SkuColumns = sku;
            _context.ParameterNames.Add(pn);
            _context.SaveChanges();
            return pn;
        }

        private List<string> GetGtinListByTenant(TenantUnit tenant)
        {
            int? id = GetNearestTenantWithMasterData(tenant);
            if (id != null)
                return _context.Products.Where(p => p.TenantUnitId == id).Select(p => p.Gtin).ToList();
            return new List<string>();
        }

        internal int? GetNearestTenantWithMasterData(TenantUnit tenant)
        {
            int? result = null;
            bool hasMasterData;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
            {
                hasMasterData = _context.Products.Any(a => a.TenantUnitId == tenant.ParentId);
                scope.Complete();
            }
            if (!hasMasterData)
            {
                if (tenant.ParentId != null)
                {
                    tenant = _context.TenantUnits.SingleOrDefault(tu => tu.Id == tenant.ParentId);
                    result = GetNearestTenantWithMasterData(tenant);
                }
            }
            else
            {
                result = tenant.ParentId;
            }

            return result;
        }

        private List<string> GetCompanyPrefixesByTenant(TenantUnit tenant)
        {
            int? tenantId = GetNearestTenantWithMasterData(tenant);

            if (tenantId != null)
            {
                Article article;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
                {
                    article = _context.Articles.Include(a => a.ParameterName).FirstOrDefault(a => a.TenantUnitId == tenantId);
                    scope.Complete();
                }
                string parameters = article.ParameterName.List.Replace(" ", "");
                var index = parameters.IndexOf("CompanyPrefixLength", StringComparison.InvariantCultureIgnoreCase);
                if (index > -1)
                {
                    string substr = parameters.Substring(0, index);
                    int semicolons = substr.Count(c => c == ';');
                    if ((substr.IndexOf("Gtin", StringComparison.InvariantCultureIgnoreCase) > -1)  || (substr.IndexOf("Upc", StringComparison.InvariantCultureIgnoreCase) > -1) )
                        --semicolons;
                    int.TryParse(article.Parameters.Split(';')[semicolons], out _companyPrefixLength);
                }
                else
                    int.TryParse(ConfigurationManager.AppSettings["CompanyPrefixLength"], out _companyPrefixLength);

                List<string> prefixes = new List<string>();
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {Timeout = new TimeSpan(0, 1, 0), IsolationLevel = IsolationLevel.ReadUncommitted}))
                {
                    prefixes = _context.Products.Where(p => p.TenantUnitId == tenantId).Select(p => p.Gtin.Substring(0, _companyPrefixLength)).Distinct().ToList();
                    scope.Complete();
                }
                return prefixes;
            }

            return new List<string>();
        }

        private List<string> GtinStringToList(string gtinString)
        {
            List<string> gtinList = gtinString.TrimStart('[').TrimEnd(']').Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            for (int i = 0; i < gtinList.Count; i++)
                gtinList[i] = gtinList[i].Trim('"');
            return gtinList;
        }


        public TenantOperation IsMasterDataImporting(int tenantId)
        {
            TenantOperation tenantOperation = _context.TenantOperations.FirstOrDefault(to=>to.TenantUnitId == tenantId && to.Operation == (int)TenantOperationType.ImportMasterData);
            return tenantOperation;
        }
    }

    internal class CsvValues
    {
        public List<string[]> Parameters { get; set; }
        public bool IsNew { get; set; }
    }
}

