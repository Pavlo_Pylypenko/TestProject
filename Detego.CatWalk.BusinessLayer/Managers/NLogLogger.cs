﻿using System;
using NLog;
using ILogger = Detego.CatWalk.BusinessLayer.Interfaces.ILogger;

namespace Detego.CatWalk.BusinessLayer.Managers
{
    public class NLogLogger : ILogger
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public NLogLogger(int? userId, string functionName)
        {
            if (userId != null)
                MappedDiagnosticsContext.Set("user_id", userId.ToString());

            if (functionName!=null)
                MappedDiagnosticsContext.Set("function_name", functionName);
        }

        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Trace(string message)
        {
            _logger.Trace(message);
        }

        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Warn(string message)
        {
            _logger.Warn(message);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void ErrorException(string message, Exception exception)
        {
            _logger.Error(exception, message);
        }

        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }

        public void Fatal(string message, Exception exception)
        {
            _logger.Fatal(exception, message);
        }

        public static Exception GetException(Exception exp)
        {
            if (exp.InnerException == null)
            {
                return exp;
            }

            return GetException(exp.InnerException);
        }
    }
}
