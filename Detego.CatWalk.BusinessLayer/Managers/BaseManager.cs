﻿using System;
using System.Transactions;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Managers
{
    public class BaseManager
    {
        protected const char MASTERDATA_DELIMITER = ';';
        protected DetegoCatWalkDbContext _context = new DetegoCatWalkDbContext();

        public BaseManager()
        {
            _context.Database.CommandTimeout = 360;
        }

        protected static void OverrideTransactionScopeMaximumTimeout(TimeSpan timeOut)
        {
            Type oSystemType = typeof(TransactionManager);

            System.Reflection.FieldInfo oCachedMaxTimeout = oSystemType.GetField("_cachedMaxTimeout", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            System.Reflection.FieldInfo oMaximumTimeout = oSystemType.GetField("_maximumTimeout", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            oCachedMaxTimeout.SetValue(null, true);
            oMaximumTimeout.SetValue(null, timeOut);
        }
    }
}
