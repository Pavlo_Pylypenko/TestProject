﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading;
using System.Transactions;
using Detego.CatWalk.BusinessLayer.Helpers;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Models;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.BusinessLayer.Managers
{
    public class TenantManager : BaseManager, ITenantManager
    {
        public List<string> GetSiteByUser(string userEmail)
        {
            User user = _context.Users.SingleOrDefault(u => u.Email.ToLower() == userEmail.ToLower());
            List<string> sites = new List<string>();
            if (user != null)
            {
                sites = GetTenants(user.TenantUnitId, (int)TenantUnitType.Site).Select(s => s.Name).ToList();
            }

            return sites;
        }

        public Site GetSite(string gln)
        {
            var site = new Site();
            TenantUnit tenant = _context.TenantUnits.Include(tu => tu.TenantUnits1).SingleOrDefault(tu => tu.Name == gln && tu.TenantUnitType == (int)TenantUnitType.Site && !tu.Deleted);

            if (tenant != null)
            {
                site.Gln = tenant.Name;
                site.Locations = new List<string>();
                foreach (var location in tenant.TenantUnits1)
                {
                   site.Locations.Add(location.Name); 
                }
            }
            return site;
        }

        public TenantUnit[] GetRootTenants()
        {
            return _context.TenantUnits.Where(t => t.ParentId == null && !t.Deleted).ToArray();
        }

        public TenantUnit[] GetChildTenants(int parentId)
        {
            var result = _context.TenantUnits.Where(t => t.ParentId == parentId && !t.Deleted).Include(t => t.Address).ToArray();
            return result;
        }

        public TenantUnit[] GetChildTenants(int parentId, int userTenantId)
        {
            var children = _context.TenantUnits.Where(t => t.ParentId == parentId && !t.Deleted).Include(t => t.Address).ToArray();
            var childrenIds = children.Select(t => t.Id).ToArray();
            var userTenant = _context.TenantUnits.FirstOrDefault(t => t.Id == userTenantId && !t.Deleted);
            while (userTenant != null && !childrenIds.Contains(userTenant.Id))
                userTenant = _context.TenantUnits.FirstOrDefault(t => t.Id == userTenant.ParentId && !t.Deleted);

            return userTenant == null ? children: new [] { userTenant };
        }

        public int AddTenant(TenantUnit tenant)
        {
            try
            {
                int unique = CheckUnique(tenant);
                if (unique != 0)
                    return unique;

                _context.TenantUnits.Add(tenant);
                _context.SaveChanges();

                if (tenant.TenantUnitType != (int)TenantUnitType.Location)
                {
                    CreateUserForTenant(tenant);
                }
            }
            catch (DbEntityValidationException e)
            {
                NLogLogger logger = new NLogLogger(null, "AddTenant");

                foreach (var eve in e.EntityValidationErrors)
                {
                    logger.Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            return tenant.Id;
        }

        internal void CreateUserForTenant(TenantUnit tenant)
        {
            string templateString;
            User user;
            int roleId;

            switch (tenant.TenantUnitType)
            {
                case (int)TenantUnitType.Label:
                    templateString = ConfigurationManager.AppSettings["AdminTemplate"];
                    roleId = RoleHelper.Roles.Single(x => x.Value == RoleHelper.Admin).Key;
                    if (string.IsNullOrEmpty(templateString))
                        return;
                    user = CreateUser(tenant, templateString, roleId);
                    break;
                case (int)TenantUnitType.Partner:
                    templateString = ConfigurationManager.AppSettings["RegionalManagerTemplate"];
                    roleId = RoleHelper.Roles.Single(x => x.Value == RoleHelper.RegionalManager).Key;
                    if (string.IsNullOrEmpty(templateString))
                        return;
                    user = CreateUser(tenant, templateString, roleId);
                    break;
                case (int)TenantUnitType.Site:
                    templateString = ConfigurationManager.AppSettings["StoreManagerTemplate"];
                    roleId = RoleHelper.Roles.Single(x => x.Value == RoleHelper.StoreManager).Key;
                    if (string.IsNullOrEmpty(templateString))
                        return;
                    user = CreateUser(tenant, templateString, roleId);
                    break;
                default:
                    return;
            }

            _context.Users.Add(user);
            _context.SaveChanges();
        }

        private static User CreateUser(TenantUnit tenant, string templateString, int roleId)
        {
            string userEmail;
            User user;
            userEmail = string.Format(templateString, string.Concat(tenant.Name.Replace(" ", string.Empty), "Id", tenant.Id));
            if (userEmail.Length > 64)
            {
                userEmail = userEmail.Substring(0, 64);
            }
            string password = ConfigurationManager.AppSettings["DefaultPassword"];
            if (string.IsNullOrEmpty(password))
                password = "test1234";

            user = new User
            {
                UserName = userEmail,
                Password = PasswordHelper.CalculateMD5Hash(password),
                RoleId = roleId,
                DateCreated = DateTime.Now.Date,
                Email = userEmail,
                TenantUnitId = tenant.Id,
                FailedLogins = 0
            };
            return user;
        }

        public int EditTenant(TenantUnit tenant)
        {
            try
            {
                TenantUnit dbEntry = _context.TenantUnits.Include(t => t.Address).SingleOrDefault(t => t.Id == tenant.Id && !t.Deleted);

                tenant.ParentId = dbEntry.ParentId;
                int unique = CheckUnique(tenant);
                if (unique != 0)
                    return unique; 

                if (dbEntry != null)
                {
                    dbEntry.Name = tenant.Name;
                    if (tenant.Address != null)
                    {
                        dbEntry.Address.AddressLine1 = tenant.Address.AddressLine1;
                        dbEntry.Address.AddressLine2 = tenant.Address.AddressLine2;
                        dbEntry.Address.Zip = tenant.Address.Zip;
                        dbEntry.Address.City = tenant.Address.City;
                        dbEntry.Address.State = tenant.Address.State;
                        dbEntry.Address.Country = tenant.Address.Country;
                        dbEntry.Address.Description = tenant.Address.Description;
                    }                  

                    _context.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                NLogLogger logger = new NLogLogger(null, "EditTenant");

                foreach (var eve in e.EntityValidationErrors)
                {
                    logger.Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            return tenant.Id;
        }

        public bool DeleteTenant(int id)
        {
            TimeSpan timeout = new TimeSpan(0, 30, 0);
            OverrideTransactionScopeMaximumTimeout(timeout);
            NLogLogger logger = new NLogLogger(null, "DeleteTenant");
            try
            {
                TransactionOptions to = new TransactionOptions();
                to.Timeout = timeout;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, to))
                {
                    DeleteTenantItem(id);
                    _context.SaveChanges();
                    //_context.ParameterNames.RemoveRange(_context.ParameterNames.Where(pn => pn.Articles.Count == 0));
                    //_context.SaveChanges();
                    scope.Complete();
                    logger.Info(string.Format("Tenant with Id={0} was  successfully deleted.", id));
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Tenant with Id={0} wasn't deleted. Reason: {1}", id, NLogLogger.GetException(ex).Message));
                return false;
            }
            return true;
        }

        public TenantUnit GetRootTenant(int? id)
        {
            TenantUnit current = _context.TenantUnits.FirstOrDefault(t => t.Id == id && !t.Deleted);
            while (current.ParentId != null)
            {
                current = _context.TenantUnits.FirstOrDefault(t => t.Id == current.ParentId);
            } 
            return current;
        }

        public TenantUnit[] GetTenants(int? tenantId, int tenantUnitType)
        {
            List<TenantUnit> tenants = new List<TenantUnit>();
            if (tenantId == null)
            {
                var res = _context.TenantUnits.Where(t => t.TenantUnitType == tenantUnitType && !t.Deleted).ToList();
                foreach (var item in res)
                    tenants.Add(new TenantUnit { Id = item.Id, Name = item.Name, TenantUnitType = item.TenantUnitType });
                return tenants.ToArray();
            }

            TenantUnit tenant = _context.TenantUnits.Single(t => t.Id == tenantId && !t.Deleted);

            if (tenant.TenantUnitType == tenantUnitType)
            {
                tenants.Add(new TenantUnit {Id = tenant.Id, Name = tenant.Name, TenantUnitType = tenant.TenantUnitType});
            }
            AddChildren(tenant.Id, tenantUnitType, tenants);
            return tenants.ToArray();
        }

        private void AddChildren(int tenantId, int tenantUnitType, List<TenantUnit> tenants)
        {
            TenantUnit[] children = _context.TenantUnits.Where(t => t.ParentId == tenantId && !t.Deleted).ToArray();

            foreach (var tenant in children)
            {
                if (tenant.TenantUnitType == tenantUnitType)
                {
                    tenants.Add(new TenantUnit {Id = tenant.Id, Name = tenant.Name, TenantUnitType = tenant.TenantUnitType});
                }
                if (tenant.TenantUnitType <= tenantUnitType)
                {
                    AddChildren(tenant.Id, tenantUnitType, tenants);
                }
            }
        }

        private void DeleteTenantItem(int id)
        {
            TenantUnit tenant = _context.TenantUnits.SingleOrDefault(t => t.Id == id);
            if (tenant != null)
            {
                List<TenantUnit> allChildren = _context.TenantUnits.Where(t => t.ParentId == tenant.Id).ToList();
                foreach (var child in allChildren)
                {
                    DeleteTenantItem(child.Id);
                }
                //Address address = _context.Addresses.SingleOrDefault(a => a.Id == tenant.AddressId);
                //if (address != null)
                //{
                //    _context.Addresses.Remove(address);
                //}

                //List<User> tenantUsers = _context.Users.Where(u => u.TenantUnitId == tenant.Id).ToList();
                //foreach (var user in tenantUsers)
                //{
                //    _context.Users.Remove(user);
                //}

                //List<StockTake> stockTakes = _context.StockTakes.Where(st => st.TenantUnitId == tenant.Id).ToList();
                //foreach (var stockTake in stockTakes)
                //{
                //    _context.StockTakes.Remove(stockTake);
                //}

                tenant.Deleted = true;
            }
        }

        public TenantUnit GetUserLabel(User user)
        {
            var tenant = _context.TenantUnits.FirstOrDefault(t => t.Id == user.TenantUnit.Id && !t.Deleted);
            while (tenant.TenantUnitType != (int)TenantUnitType.Label)
            {
                tenant = _context.TenantUnits.FirstOrDefault(t => t.Id == tenant.ParentId && !t.Deleted);
            }
            return tenant;
        }

        private int CheckUnique(TenantUnit tenant)
        {
            if (tenant.TenantUnitType == (int)TenantUnitType.Site)
                if (_context.TenantUnits.SingleOrDefault(tu => tu.Id != tenant.Id && tu.Name == tenant.Name && tu.TenantUnitType == (int)TenantUnitType.Site && !tu.Deleted) != null)
                    return -2;
            if (tenant.TenantUnitType == (int) TenantUnitType.Location)
            {
                var site = _context.TenantUnits.Include(tu => tu.TenantUnits1).SingleOrDefault(tu => tu.Id == tenant.ParentId && !tu.Deleted);
                if (site != null && site.TenantUnits1.FirstOrDefault(tu => tu.Id != tenant.Id && tu.Name == tenant.Name && !tu.Deleted) != null) 
                    return -3;
            }
            return 0;
        }
    }
}
