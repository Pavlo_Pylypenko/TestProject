﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace PasswordToHash
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine();
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            Console.Write(sb.ToString());
            Console.ReadLine();
        }
    }
}
