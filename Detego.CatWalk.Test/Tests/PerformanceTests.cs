﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Detego.CatWalk.Test.Model;

namespace Detego.CatWalk.Test.Tests
{
    public class PerformanceTests 
    {
        public PerformanceTestResult Test1StockTake()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DetegoCatWalkDb"].ConnectionString);

            using (SqlCommand cmd = new SqlCommand("StockTakeReport", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("StockTakeIds", SqlDbType.VarChar, 1).Value = "2";

                conn.Open();
                DateTime start = DateTime.Now;
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                int count = 0;
                while (reader.Read())
                {
                    ++ count;
                    var id = reader[0].ToString();
                }
                TimeSpan timeDiff = DateTime.Now - start;

                return new PerformanceTestResult {ExecutionTime = timeDiff};
            }
        }

        public PerformanceTestResult Test10StockTakes()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DetegoCatWalkDb"].ConnectionString);
                
            using (SqlCommand cmd = new SqlCommand("StockTakeReport", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("StockTakeIds", SqlDbType.VarChar, 1000).Value = "1,2,3,4,5,6,7,8,9,10";

                conn.Open();
                DateTime start = DateTime.Now;
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int count = 0;
                while (reader.Read())
                {
                    ++count;
                    var id = reader[0].ToString();
                }
                TimeSpan timeDiff = DateTime.Now - start;

                return new PerformanceTestResult {ExecutionTime = timeDiff};
            }
        }

        public PerformanceTestResult Test1StockTakeNoGtin()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DetegoCatWalkDb"].ConnectionString);

            using (SqlCommand cmd = new SqlCommand("StockTakeReportNoGtin", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("StockTakeIds", SqlDbType.VarChar, 1).Value = "2";

                conn.Open();
                DateTime start = DateTime.Now;
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                int count = 0;
                while (reader.Read())
                {
                    ++count;
                    var id = reader[0].ToString();
                }
                TimeSpan timeDiff = DateTime.Now - start;

                return new PerformanceTestResult { ExecutionTime = timeDiff };
            }
        }

        public PerformanceTestResult Test10StockTakesNoGtin()
        {
            SqlConnection conn =
     new SqlConnection(
         ConfigurationManager.ConnectionStrings["DetegoCatWalkDb"].ConnectionString);

            using (SqlCommand cmd = new SqlCommand("StockTakeReportNoGtin", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("StockTakeIds", SqlDbType.VarChar, 1000).Value = "1,2,3,4,5,6,7,8,9,10";

                conn.Open();
                DateTime start = DateTime.Now;
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int count = 0;
                while (reader.Read())
                {
                    ++count;
                    var id = reader[0].ToString();
                }
                TimeSpan timeDiff = DateTime.Now - start;

                return new PerformanceTestResult { ExecutionTime = timeDiff };
            }
        }
    }
}
