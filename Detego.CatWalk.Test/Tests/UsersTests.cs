﻿using System.Collections.Generic;
using System.Linq;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.DataLayer;

namespace Detego.CatWalk.Test.Tests
{
    public class UserTests
    {
        public List<User> GetUsers()
        {
             IUserManager um = new UserManager();
             return um.GetUserList(0, 0).ToList();
        }
    }
}
