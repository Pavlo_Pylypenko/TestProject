﻿using Detego.CatWalk.BusinessLayer.Helpers;

namespace Detego.CatWalk.Test.Tests
{
    public class EpcHelperTests
    {
        public string TestEpcToGtin(string epc)
        {
            EpcHelper epcHelper = new EpcHelper();
            return epcHelper.GetEAN13(epc);
        }
    }
}
