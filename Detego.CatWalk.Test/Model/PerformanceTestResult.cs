﻿using System;

namespace Detego.CatWalk.Test.Model
{
    /// <summary>
    /// Performance Test Result abstract class
    /// </summary>
    public class PerformanceTestResult
    {
        public TimeSpan ExecutionTime { get; set; }

        public TimeSpan DelayWorkTime { get; set; }
    }
}