﻿using System.Diagnostics;
using Detego.CatWalk.Test.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Detego.CatWalk.Test.UnitTests
{
    [TestClass]
    public class EpcHelperUnitTests
    {
        private EpcHelperTests _tests = new EpcHelperTests();

        [TestMethod]
        public void TestConvertEpcToGtin()
        {
            string result = _tests.TestEpcToGtin("3034F77AD406F69000012B1F");
            Trace.Write("result: " + result);
            Assert.IsTrue(result == "4054709071300");
        }

    }
}