﻿using System;
using Detego.CatWalk.BusinessLayer.Managers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Detego.CatWalk.Test.UnitTests
{
    [TestClass]
    public class BusinessLogicUnitTests
    {
        [TestMethod]
        public void Test1()
        {
            var stm = new ReportManager();
            try
            {
                var res = stm.GetReport(3, new DateTime(2016, 7, 1));
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }
    }
}
