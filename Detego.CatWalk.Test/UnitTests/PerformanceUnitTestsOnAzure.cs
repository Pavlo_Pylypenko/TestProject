﻿using System;
using System.Diagnostics;
using Detego.CatWalk.Test.Model;
using Detego.CatWalk.Test.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Detego.CatWalk.Test.UnitTests
{
    class PerformanceUnitTestsOnAzure
    {
        private PerformanceTests _tests = new PerformanceTests();

        [TestMethod]
        public void Test1StockTake()
        {
            PerformanceTestResult result = _tests.Test1StockTake();
            Trace.Write("Execution time: " + result.ExecutionTime);
            Assert.IsTrue(result.ExecutionTime < new TimeSpan(0, 0, 0, 0, 500));
        }

        [TestMethod]
        public void Test10StockTakes()
        {
            PerformanceTestResult result = _tests.Test10StockTakes();
            Trace.Write("Execution time: " + result.ExecutionTime);
            Assert.IsTrue(result.ExecutionTime < new TimeSpan(0, 0, 0, 0, 500));
        }
    }
}
