﻿using System.Net;
using System.Net.Mail;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Detego.CatWalk.Test.UnitTests
{
    [TestClass]
    public class EmailTests
    {
        [TestMethod]
        public void SendEmail()
        {
            var from = "catwalk@detego.com";
            var to = "pylpav@hotmail.com";

            var mail = new MailMessage();
            mail.From = new MailAddress(from, "Detego");


            mail.Subject = "Resetting your Detego CatWalk password";
            StringBuilder body = new StringBuilder();
            body.AppendLine(string.Format("Dear {0}", "Bogdan"));
            body.AppendLine("");
            body.AppendLine("You (or somebody else) requested to reset the password for your Detego CatWalk user account.");
            body.AppendLine("If you don't want  to reset the password, you can simply ignore this email.");
            body.AppendLine("");
            body.AppendLine(string.Format("To reset your password, please use this link: {0}", "123"));
            body.AppendLine("Sincerely,");
            body.AppendLine("Detego CatWalk,");
            mail.Body = body.ToString();


            mail.To.Add(new MailAddress(to, "Bogdan"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
           // smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("detegomailsender@gmail.com", "detegomailsender123");
            smtp.EnableSsl = true;

            smtp.SendCompleted += (s, e) =>
            {
                smtp.Dispose();
            };
            smtp.Send(mail);
        }
    }
}
