﻿using System.Collections.Generic;

namespace Detego.CatWalk.Web.Models
{
    public class StockTakeInfo
    {
        public string Site { get; set; }

        public string Location { get; set; }

        public string Readpoint { get; set; }

        public string User { get; set; }

        public List<string> EpcList { get; set; }

        public int TimeZone { get; set; }
    }
}