﻿using System;

namespace Detego.CatWalk.Web.Models
{
    public class DayReportParameters
    {
        public int SiteId { get; set; }

        public DateTime Date { get; set; }
    }
}