﻿using System.Web.Mvc;

namespace Detego.CatWalk.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}