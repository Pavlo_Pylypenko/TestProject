﻿using System;
using System.Web.Mvc;

namespace Detego.CatWalk.Web.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChangePassword(string email, string token)
        {           
            return View(Tuple.Create(email, token));
        }
    }
}