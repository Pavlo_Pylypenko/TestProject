﻿using System;
using System.Web.Mvc;
using Detego.CatWalk.Web.Models;

namespace Detego.CatWalk.Web.Controllers
{
    public class StockTakesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Report(int id)
        {
            return View(id);
        }

        [Route("Stocktakes/Report/{id}/{date}")]
        public ActionResult DayReport(int id, DateTime date)
        {
            return View(new DayReportParameters { SiteId = id, Date = date });
        }
    }
}