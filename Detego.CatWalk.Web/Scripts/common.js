﻿﻿ApiBaseUrl = "https://localhost:44301/api/";

function getAddress(address) {
    var result = "";

    if (address.City.length > 0)
        result += address.City;
    if (address.AddressLine1.length > 0)
        if (result.length > 0)
            result += ", " + address.AddressLine1;
        else
            result += address.AddressLine1;
    if (address.Description.length > 0)
        if (result.length > 0)
            result += ", " + address.Description;
        else
            result += address.Description;

    if (result.length > 0)
        result = "(" + result + ")";

    return result;
}

$.support.cors = true;
$.ajaxSetup({
    headers: {
        "Authorization": window.localStorage.authorizationString
    },
    beforeSend: function (xhr) {
        if (!checkLogged()) {
            xhr.abort();
            clearUserData();
            checkVisit();
        }
           
        xhr.setRequestHeader('Authorization', window.localStorage.authorizationString)
    }
});

function saveAuth(authString) {
    window.localStorage.setItem("authorizationString", authString);
}

function saveEmail(email) {
    window.localStorage.setItem("email", email);
}

function getEmail() {
    return window.localStorage.email;
}

function getUserRole() {
    if (!window.localStorage.role) {
        $.ajax({
            url: ApiBaseUrl + "UserRole",
            method: "GET",
            success: function(result) {
                window.localStorage.setItem("role", result);
            }
        });
    }
     return window.localStorage.role;
}

function getUserRole2(callback) {
    if (!window.localStorage.role) {
        $.ajax({
            url: ApiBaseUrl + "UserRole",
            method: "GET",
            success: function(result) {
                window.localStorage.setItem("role", result);
                callback();
            }
        });
    }
     return window.localStorage.role;
}

function checkLogged() {
    return (window.localStorage.authorizationString != null);
}

function clearUserData() {
    window.localStorage.removeItem("role");
    window.localStorage.removeItem("authorizationString");
    window.localStorage.removeItem("email");
}

function checkVisit() {
    if (window.localStorage.authorizationString == null) {
        if (SiteUrl.slice(-1) != '/')
            SiteUrl += '/';
        window.location = SiteUrl + "Login/Index";
    };
}