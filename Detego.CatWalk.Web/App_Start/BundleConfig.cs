﻿using System.Web.Optimization;

namespace Detego.CatWalk.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/common.js", 
                        "~/Scripts/modernizr-2.6.2.js", 
                        "~/Scripts/moment.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap-surveyor.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/kendo.detego.css",
                      "~/Content/Site.css"));
        }
    }
}