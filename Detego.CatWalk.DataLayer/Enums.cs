﻿namespace Detego.CatWalk.DataLayer
{
    public enum TenantUnitType
    {
        Label = 1,
        Partner = 2,
        Site = 3, 
        Location = 4
    }

    public enum TenantOperationType
    {
        ImportMasterData = 1,
        ImportImages = 2,
    }

    public enum OperationStatus
    {
        Started = 1,
        Completed = 2,
    }
}
