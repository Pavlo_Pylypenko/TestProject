
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/14/2016 20:47:34
-- Generated from EDMX file: D:\Projects\Detego-CatWalk\Detego.CatWalk.DataLayer\DetegoCatWalkDb.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO

IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Articles_ParameterNames]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Articles] DROP CONSTRAINT [FK_Articles_ParameterNames];
GO
IF OBJECT_ID(N'[dbo].[FK_ProductArticle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Products] DROP CONSTRAINT [FK_ProductArticle];
GO
IF OBJECT_ID(N'[dbo].[FK_StockTakeEPCItems_Products]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StockTakeEPCItems] DROP CONSTRAINT [FK_StockTakeEPCItems_Products];
GO
IF OBJECT_ID(N'[dbo].[FK_StockTakeEPCItems_StockTakes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StockTakeEPCItems] DROP CONSTRAINT [FK_StockTakeEPCItems_StockTakes];
GO
IF OBJECT_ID(N'[dbo].[FK_StockTakes_TenantUnits]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StockTakes] DROP CONSTRAINT [FK_StockTakes_TenantUnits];
GO
IF OBJECT_ID(N'[dbo].[FK_StockTakes_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StockTakes] DROP CONSTRAINT [FK_StockTakes_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_TenantUnitArticle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Articles] DROP CONSTRAINT [FK_TenantUnitArticle];
GO
IF OBJECT_ID(N'[dbo].[FK_TenantUnitProduct]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Products] DROP CONSTRAINT [FK_TenantUnitProduct];
GO
IF OBJECT_ID(N'[dbo].[FK_TenantUnits_Addresses]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TenantUnits] DROP CONSTRAINT [FK_TenantUnits_Addresses];
GO
IF OBJECT_ID(N'[dbo].[FK_TenantUnits_TenantUnits]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TenantUnits] DROP CONSTRAINT [FK_TenantUnits_TenantUnits];
GO
IF OBJECT_ID(N'[dbo].[FK_TenantUnitTenantOperations]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TenantOperations] DROP CONSTRAINT [FK_TenantUnitTenantOperations];
GO
IF OBJECT_ID(N'[dbo].[FK_Users_Roles]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Roles];
GO
IF OBJECT_ID(N'[dbo].[FK_Users_TenantUnits]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_TenantUnits];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Addresses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Addresses];
GO
IF OBJECT_ID(N'[dbo].[Articles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Articles];
GO
IF OBJECT_ID(N'[dbo].[ParameterNames]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ParameterNames];
GO
IF OBJECT_ID(N'[dbo].[Products]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Products];
GO
IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO
IF OBJECT_ID(N'[dbo].[StockTakeEPCItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StockTakeEPCItems];
GO
IF OBJECT_ID(N'[dbo].[StockTakes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StockTakes];
GO
IF OBJECT_ID(N'[dbo].[TenantOperations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TenantOperations];
GO
IF OBJECT_ID(N'[dbo].[TenantUnits]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TenantUnits];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Addresses'
CREATE TABLE [dbo].[Addresses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [City] nvarchar(64)  NULL,
    [AddressLine1] nvarchar(256)  NULL,
    [AddressLine2] nvarchar(256)  NULL,
    [Zip] nvarchar(9)  NULL,
    [State] nvarchar(64)  NULL,
    [Country] nvarchar(32)  NULL,
    [Description] nvarchar(1024)  NULL
);
GO

-- Creating table 'Articles'
CREATE TABLE [dbo].[Articles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TenantUnitId] int  NOT NULL,
    [ParameterNamesId] int  NOT NULL,
    [Parameters] nvarchar(max)  NULL,
    [GtinList] nvarchar(max)  NULL,
    [Sku] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ParameterNames'
CREATE TABLE [dbo].[ParameterNames] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [List] nvarchar(max)  NOT NULL,
    [SkuColumns] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Products'
CREATE TABLE [dbo].[Products] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Gtin] varchar(14)  NOT NULL,
    [ArticleId] int  NOT NULL,
    [TenantUnitId] int  NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(64)  NOT NULL
);
GO

-- Creating table 'StockTakeEPCItems'
CREATE TABLE [dbo].[StockTakeEPCItems] (
    [StockTakeId] int  NOT NULL,
    [EPC] nvarchar(24)  NOT NULL,
    [ProductId] int  NOT NULL
);
GO

-- Creating table 'StockTakes'
CREATE TABLE [dbo].[StockTakes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Number] nvarchar(30)  NOT NULL,
    [Start] datetime  NOT NULL,
    [First] datetime  NULL,
    [Last] datetime  NULL,
    [End] datetime  NULL,
    [TenantUnitId] int  NOT NULL,
    [Readpoint] nvarchar(64)  NULL,
    [UserId] int  NULL,
    [TimeZone] int  NOT NULL,
    [Quantity] int  NOT NULL
);
GO

-- Creating table 'TenantOperations'
CREATE TABLE [dbo].[TenantOperations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Operation] int  NOT NULL,
    [OperationStatus] int  NOT NULL,
    [TenantUnitId] int  NOT NULL,
    [Count] int  NULL,
    [DateCompleted] datetime  NULL
);
GO

-- Creating table 'TenantUnits'
CREATE TABLE [dbo].[TenantUnits] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(64)  NOT NULL,
    [ParentId] int  NULL,
    [AddressId] int  NULL,
    [TenantUnitType] int  NOT NULL,
    [Deleted] bit  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(64)  NOT NULL,
    [Password] nvarchar(64)  NOT NULL,
    [TenantUnitId] int  NULL,
    [Email] nvarchar(64)  NULL,
    [DateCreated] datetime  NOT NULL,
    [Deactivated] bit  NOT NULL,
    [RoleId] int  NOT NULL,
    [LastSignIn] datetime  NULL,
    [Phone] nvarchar(64)  NULL,
    [Fax] nvarchar(64)  NULL,
    [EmployeeId] nvarchar(64)  NULL,
    [FailedLogins] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [PK_Addresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Articles'
ALTER TABLE [dbo].[Articles]
ADD CONSTRAINT [PK_Articles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ParameterNames'
ALTER TABLE [dbo].[ParameterNames]
ADD CONSTRAINT [PK_ParameterNames]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [PK_Products]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [StockTakeId], [EPC] in table 'StockTakeEPCItems'
ALTER TABLE [dbo].[StockTakeEPCItems]
ADD CONSTRAINT [PK_StockTakeEPCItems]
    PRIMARY KEY CLUSTERED ([StockTakeId], [EPC] ASC);
GO

-- Creating primary key on [Id] in table 'StockTakes'
ALTER TABLE [dbo].[StockTakes]
ADD CONSTRAINT [PK_StockTakes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TenantOperations'
ALTER TABLE [dbo].[TenantOperations]
ADD CONSTRAINT [PK_TenantOperations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TenantUnits'
ALTER TABLE [dbo].[TenantUnits]
ADD CONSTRAINT [PK_TenantUnits]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AddressId] in table 'TenantUnits'
ALTER TABLE [dbo].[TenantUnits]
ADD CONSTRAINT [FK_TenantUnits_Addresses]
    FOREIGN KEY ([AddressId])
    REFERENCES [dbo].[Addresses]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TenantUnits_Addresses'
CREATE INDEX [IX_FK_TenantUnits_Addresses]
ON [dbo].[TenantUnits]
    ([AddressId]);
GO

-- Creating foreign key on [ParameterNamesId] in table 'Articles'
ALTER TABLE [dbo].[Articles]
ADD CONSTRAINT [FK_Articles_ParameterNames]
    FOREIGN KEY ([ParameterNamesId])
    REFERENCES [dbo].[ParameterNames]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Articles_ParameterNames'
CREATE INDEX [IX_FK_Articles_ParameterNames]
ON [dbo].[Articles]
    ([ParameterNamesId]);
GO

-- Creating foreign key on [ArticleId] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [FK_ProductArticle]
    FOREIGN KEY ([ArticleId])
    REFERENCES [dbo].[Articles]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductArticle'
CREATE INDEX [IX_FK_ProductArticle]
ON [dbo].[Products]
    ([ArticleId]);
GO

-- Creating foreign key on [TenantUnitId] in table 'Articles'
ALTER TABLE [dbo].[Articles]
ADD CONSTRAINT [FK_TenantUnitArticle]
    FOREIGN KEY ([TenantUnitId])
    REFERENCES [dbo].[TenantUnits]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TenantUnitArticle'
CREATE INDEX [IX_FK_TenantUnitArticle]
ON [dbo].[Articles]
    ([TenantUnitId]);
GO

-- Creating foreign key on [ProductId] in table 'StockTakeEPCItems'
ALTER TABLE [dbo].[StockTakeEPCItems]
ADD CONSTRAINT [FK_StockTakeEPCItems_Products]
    FOREIGN KEY ([ProductId])
    REFERENCES [dbo].[Products]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StockTakeEPCItems_Products'
CREATE INDEX [IX_FK_StockTakeEPCItems_Products]
ON [dbo].[StockTakeEPCItems]
    ([ProductId]);
GO

-- Creating foreign key on [TenantUnitId] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [FK_TenantUnitProduct]
    FOREIGN KEY ([TenantUnitId])
    REFERENCES [dbo].[TenantUnits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TenantUnitProduct'
CREATE INDEX [IX_FK_TenantUnitProduct]
ON [dbo].[Products]
    ([TenantUnitId]);
GO

-- Creating foreign key on [RoleId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_Users_Roles]
    FOREIGN KEY ([RoleId])
    REFERENCES [dbo].[Roles]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Users_Roles'
CREATE INDEX [IX_FK_Users_Roles]
ON [dbo].[Users]
    ([RoleId]);
GO

-- Creating foreign key on [StockTakeId] in table 'StockTakeEPCItems'
ALTER TABLE [dbo].[StockTakeEPCItems]
ADD CONSTRAINT [FK_StockTakeEPCItems_StockTakes]
    FOREIGN KEY ([StockTakeId])
    REFERENCES [dbo].[StockTakes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TenantUnitId] in table 'StockTakes'
ALTER TABLE [dbo].[StockTakes]
ADD CONSTRAINT [FK_StockTakes_TenantUnits]
    FOREIGN KEY ([TenantUnitId])
    REFERENCES [dbo].[TenantUnits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StockTakes_TenantUnits'
CREATE INDEX [IX_FK_StockTakes_TenantUnits]
ON [dbo].[StockTakes]
    ([TenantUnitId]);
GO

-- Creating foreign key on [UserId] in table 'StockTakes'
ALTER TABLE [dbo].[StockTakes]
ADD CONSTRAINT [FK_StockTakes_Users]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StockTakes_Users'
CREATE INDEX [IX_FK_StockTakes_Users]
ON [dbo].[StockTakes]
    ([UserId]);
GO

-- Creating foreign key on [TenantUnitId] in table 'TenantOperations'
ALTER TABLE [dbo].[TenantOperations]
ADD CONSTRAINT [FK_TenantUnitTenantOperations]
    FOREIGN KEY ([TenantUnitId])
    REFERENCES [dbo].[TenantUnits]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TenantUnitTenantOperations'
CREATE INDEX [IX_FK_TenantUnitTenantOperations]
ON [dbo].[TenantOperations]
    ([TenantUnitId]);
GO

-- Creating foreign key on [ParentId] in table 'TenantUnits'
ALTER TABLE [dbo].[TenantUnits]
ADD CONSTRAINT [FK_TenantUnits_TenantUnits]
    FOREIGN KEY ([ParentId])
    REFERENCES [dbo].[TenantUnits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TenantUnits_TenantUnits'
CREATE INDEX [IX_FK_TenantUnits_TenantUnits]
ON [dbo].[TenantUnits]
    ([ParentId]);
GO

-- Creating foreign key on [TenantUnitId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_Users_TenantUnits]
    FOREIGN KEY ([TenantUnitId])
    REFERENCES [dbo].[TenantUnits]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Users_TenantUnits'
CREATE INDEX [IX_FK_Users_TenantUnits]
ON [dbo].[Users]
    ([TenantUnitId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------