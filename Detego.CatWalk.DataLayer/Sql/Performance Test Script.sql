TRUNCATE TABLE [StockTakeEPCItems]
GO

IF OBJECT_ID(N'[dbo].[FK_StockTakeEPCItems_StockTakes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StockTakeEPCItems] DROP CONSTRAINT [FK_StockTakeEPCItems_StockTakes];
GO

TRUNCATE TABLE [StockTakes]
GO


-- Creating foreign key on [StockTakeId] in table 'StockTakeEPCItems'
ALTER TABLE [dbo].[StockTakeEPCItems]
ADD CONSTRAINT [FK_StockTakeEPCItems_StockTakes]
    FOREIGN KEY ([StockTakeId])
    REFERENCES [dbo].[StockTakes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

DELETE FROM [Users]
GO
DELETE FROM [Roles]
GO

IF OBJECT_ID(N'[dbo].[FK_StockTakeEPCItems_Products]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StockTakeEPCItems] DROP CONSTRAINT [FK_StockTakeEPCItems_Products];
GO

TRUNCATE TABLE [Products]
GO

-- Creating foreign key on [ProductId] in table 'StockTakeEPCItems'
ALTER TABLE [dbo].[StockTakeEPCItems]
ADD CONSTRAINT [FK_StockTakeEPCItems_Products]
    FOREIGN KEY ([ProductId])
    REFERENCES [dbo].[Products]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;


IF OBJECT_ID(N'[dbo].[FK_ProductArticle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Products] DROP CONSTRAINT [FK_ProductArticle];
GO

TRUNCATE TABLE [Articles]
GO

-- Creating foreign key on [ArticleId] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [FK_ProductArticle]
    FOREIGN KEY ([ArticleId])
    REFERENCES [dbo].[Articles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;


DELETE FROM [ParameterNames]
GO

DELETE FROM [TenantUnits]
GO
DELETE FROM [Addresses]
GO

DBCC CHECKIDENT ('TenantUnits', RESEED, 1)
GO
DBCC CHECKIDENT ('Articles', RESEED, 1)
GO
DBCC CHECKIDENT ('ParameterNames', RESEED, 1)
GO
DBCC CHECKIDENT ('Products', RESEED, 1)
GO
DBCC CHECKIDENT ('Roles', RESEED, 1)
GO
DBCC CHECKIDENT ('Users', RESEED, 1)
GO
DBCC CHECKIDENT ('StockTakes', RESEED, 1)
GO
DBCC CHECKIDENT ('Addresses', RESEED, 1)
GO


SET IDENTITY_INSERT [TenantUnits] ON
GO

INSERT [TenantUnits] ([Id], [Name], [ParentId], [AddressId], [TenantUnitType]) VALUES (1, N'Label 1', NULL, NULL, 1)
GO

SET IDENTITY_INSERT [TenantUnits] OFF
GO


--Roles
SET IDENTITY_INSERT [dbo].[Roles] ON 

GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (1, N'Root Account')
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (2, N'IT Admin')
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (3, N'Regional Manager')
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (4, N'Store Manager')
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (5, N'User Account')
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
--End Roles

--Users
SET IDENTITY_INSERT [dbo].[Users] ON 

GO
INSERT [dbo].[Users] ([Id], [UserName], [Password], [TenantUnitId], [Email], [DateCreated], [Deactivated], [RoleId], [LastSignIn], [Phone], [Fax], [EmployeeId]) VALUES (1, N'Root', N'AABB2100033F0352FE7458E412495148', NULL, N'root@mail.com', CAST(N'2016-06-13 00:00:00.000' AS DateTime), 0, 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
-- End Users

--constants
DECLARE @cntPartnersPerLabel INT = 10;
DECLARE @cntSitesPerPartner INT = 100;
DECLARE @cntLocationsPerSite INT = 4;
DECLARE @cntArticlesPerPartner INT = 2000000;
DECLARE @cntParameterNamesPerArticle INT = 20;
DECLARE @cntProductsPerSite INT = 10000;
DECLARE @cntEPCPerStockTake INT = 2500;
DECLARE @cntLocationGlobal INT = 0;

--Parameter names
   Print 'Parameter names'
   DECLARE @cntParameterName INT = 0;
	--WHILE @cntParameterName < @cntParameterNamesPerArticle
	--BEGIN
		INSERT [ParameterNames] ([List], SkuColumns) VALUES ( 'ArticleNumber;Color;Size;A;B;C;D;E;F;G;H;I;J;K;L;M;N;O;P;Q;R;S;T;U;V', N'ArticleNumber;Color;Size')
		SET @cntParameterName = @cntParameterName + 1;
	--END;
	Print 'End Parameter names'
--Parameter names

Print 'Sites'
DECLARE @cntPartner INT = 0;
WHILE @cntPartner < @cntPartnersPerLabel
BEGIN
   Print 'Partner ' + Convert(nvarchar(64),@cntPartner + 1)
   DECLARE @cntPartnerId INT = 0
   INSERT [TenantUnits] ([Name], [ParentId], [AddressId], [TenantUnitType]) VALUES ('Partner ' + Convert(nvarchar(64),@cntPartner + 1), 1, NULL, 2)
   SET @cntPartnerId = Scope_Identity()
   
	If (@cntPartnerId = 2)
	BEGIN
	   --Articles
	   Print 'Articles'
	   DECLARE @cntArticle INT = 0;
		WHILE @cntArticle < @cntArticlesPerPartner/100
		BEGIN
			INSERT [Articles] ([TenantUnitId], [Sku], ParameterNamesId, Parameters) 
			VALUES (@cntPartnerId, N'Article Number 1;Color 1;Sz 1', 1, 'Article Number 1;Color 1;Sz 1;Value 4;Value 5;Value 6;Value 7;Value 8;Value 9;Value 10;Value 11;Value 12;Value 13;Value 14;Value 15;Value 16;Value 17;Value 18;Value 19;Value 20;Value 21;Value 22;Value 23;Value 24;Value 25')
			SET @cntArticle = @cntArticle + 1;
		END;

		DECLARE @cntPackage INT = 0;
		WHILE @cntPackage < 99
		BEGIN
			INSERT [Articles] ([TenantUnitId], [Sku], ParameterNamesId, Parameters) 
			SELECT  @cntPartnerId, [Sku], ParameterNamesId, Parameters
			FROM  Articles
			WHERE Articles.TenantUnitId = 2 and (Articles.Id between 1 and (@cntArticlesPerPartner/100))

			SET @cntPackage = @cntPackage + 1;
		END;

		--End Articles
		Print 'End Articles'

	END
	ELSE
	BEGIN
			INSERT [Articles] ([TenantUnitId], [Sku], ParameterNamesId, Parameters) 
			SELECT  @cntPartnerId, [Sku], ParameterNamesId, Parameters
			FROM  Articles
			WHERE Articles.TenantUnitId = 2
	END

    Print 'Products'
	--Products
	INSERT [Products] ([Gtin], [ArticleId], [TenantUnitId]) 
	SELECT right('0000000000000' + cast((Id) as varchar(13)), 13), Id, TenantUnitId
	FROM Articles
	WHERE Articles.TenantUnitId = @cntPartnerId

	--	--Products
	--INSERT [Products] ([Gtin], [ArticleId], [TenantUnitId]) 
	--SELECT right('0000000000000' + cast((Id) as varchar(13)), 13), Id, TenantUnitId
	--FROM Articles
	--WHERE Articles.TenantUnitId = @cntPartnerId

	--UPDATE [Products] 
	--SET Gtin = right('0000000000000' + cast((Id) as varchar(13)), 13)
	
	--End Products
	Print 'End Products'


Print 'Sites'
   DECLARE @cntSite INT = 0
	WHILE @cntSite < @cntSitesPerPartner
	BEGIN
	   DECLARE @cntSiteId INT = 0
	   DECLARE @cntAddressId INT = 0
	   INSERT INTO [dbo].[Addresses] ([City],[AddressLine1],[Description]) VALUES ('City '  + Convert(nvarchar(64),@cntSite + 1),'Address Line ' + Convert(nvarchar(256),@cntSite + 1),'Description '  + Convert(nvarchar(1024),@cntSite + 1))
	   SET @cntAddressId = Scope_Identity()
	   INSERT [TenantUnits] ([Name], [ParentId], [AddressId], [TenantUnitType]) VALUES (right('0000000000000' + cast(@cntAddressId as nvarchar(13)), 13), @cntPartnerId, @cntAddressId, 3)
	   SET @cntSiteId = Scope_Identity()

	    DECLARE @cntLocation INT = 0;
		DECLARE @cntStockTakeId INT = 0
		WHILE @cntLocation < @cntLocationsPerSite
		BEGIN
			DECLARE @cntLocationId INT = 0
			INSERT [TenantUnits] ([Name], [ParentId], [AddressId], [TenantUnitType]) VALUES ('Location ' + Convert(nvarchar(64),@cntLocation + 1), @cntSiteId, NULL, 4)
			SET @cntLocationId = Scope_Identity()

			--If (@cntPartnerId = 2)
			--BEGIN
			Print 'Stock Takes'
			--StockTake
			    DECLARE @cntStockTake INT = 0;
				DECLARE @cntStockTakesPerLocation INT = 1 --FLOOR(RAND()*(5-1)+1);
				WHILE @cntStockTake < @cntStockTakesPerLocation
				BEGIN
					INSERT [StockTakes] ([Number], [Start], [Last],[End], [TenantUnitId], [Readpoint], [UserId], [Quantity]) 
					VALUES (right('0000000000' + cast((@cntStockTakeId + 1) as nvarchar(10)), 10), DATEADD(day , @cntStockTake, GETDATE()), DATEADD(day , @cntStockTake, GETDATE()), DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(day , @cntStockTake, GETDATE())), '23:59:59'), @cntLocationId, N'Handheld1', 1, @cntEPCPerStockTake)
					SET @cntStockTakeId = Scope_Identity()
				
					Print 'End Stock Takes'

					Print 'StockTakeEPCItems'
					INSERT [StockTakeEPCItems] ([StockTakeId], [EPC], [ProductId]) 
					SELECT @cntStockTakeId, 'EPC ' + Convert(nvarchar(24),Id), Id --round(Id / 5, 1) + 1
					FROM Products
					WHERE Id between (@cntEPCPerStockTake*@cntLocationGlobal + 1) and  (@cntEPCPerStockTake*(@cntLocationGlobal+1)) 
					Print 'End StockTakeEPCItems'

					SET @cntStockTake = @cntStockTake + 1
				--END

			--End StockTake
			END

		    SET @cntLocation = @cntLocation + 1;
			SET @cntLocationGlobal = @cntLocationGlobal + 1;
		END;

	   SET @cntSite  = @cntSite + 1;
	END;
Print 'End Sites' 

   Print 'Partner ' + Convert(nvarchar(64),@cntPartner + 1)
   SET @cntPartner = @cntPartner + 1;
END;
Print 'End Partners'

DECLARE @i INT = 0;
WHILE (@i <= (@cntArticlesPerPartner * @cntPartnersPerLabel))
BEGIN
Update Articles
SET GtinList = dbo.GetArticleGtins(Id), Sku = 'Article Number ' + Convert(varchar(30),Id) + ';Color '  + Convert(varchar(30),Id) + ';Sz '  + Convert(varchar(30),Id), Parameters = 'Article Number ' + Convert(varchar(30),Id) + ';Color '  + Convert(varchar(30),Id) + ';Sz '  + Convert(varchar(30),Id) + ';Value 4;Value 5;Value 6;Value 7;Value 8;Value 9;Value 10;Value 11;Value 12;Value 13;Value 14;Value 15;Value 16;Value 17;Value 18;Value 19;Value 20;Value 21;Value 22;Value 23;Value 24;Value 25'
where Id between (@i+1) and (10000 + @i)

Print @i
SET @i = @i + 10000

END
GO

Print 'Update Articles completed'

--INSERT INTO TenantGtins (GtinList, TenantUnitId)
--SELECT dbo.GetTenantGtins(Id), Id
--FROM TenantUnits
--Where TenantUnitType=2

--Print 'Update TenantGtins completed'

GO

--Users

SET IDENTITY_INSERT [dbo].[Users] ON 

GO
INSERT [dbo].[Users] ([Id], [UserName], [Password], [TenantUnitId], [Email], [DateCreated], [Deactivated], [RoleId], [LastSignIn], [Phone], [Fax], [EmployeeId]) VALUES (2, N'John Smith', N'AABB2100033F0352FE7458E412495148', 1, N'johnsmith@hotmail.com', CAST(N'2016-06-23 20:34:01.430' AS DateTime), 0, 2, CAST(N'2016-06-23 20:34:01.430' AS DateTime), N'(541) 754-3010', N'1-408-999 8888', N'1')
GO
INSERT [dbo].[Users] ([Id], [UserName], [Password], [TenantUnitId], [Email], [DateCreated], [Deactivated], [RoleId], [LastSignIn], [Phone], [Fax], [EmployeeId]) VALUES (3, N'Adam Parker', N'AABB2100033F0352FE7458E412495148', 2, N'adamparker@gmail.com', CAST(N'2016-06-23 20:34:01.447' AS DateTime), 0, 3, CAST(N'2016-06-23 20:34:01.447' AS DateTime), N'(541) 744-8010', N'1-418-999 8888', N'2')
GO
INSERT [dbo].[Users] ([Id], [UserName], [Password], [TenantUnitId], [Email], [DateCreated], [Deactivated], [RoleId], [LastSignIn], [Phone], [Fax], [EmployeeId]) VALUES (4, N'Tom Rikhter', N'AABB2100033F0352FE7458E412495148', 3, N'jtomrikhter@hotmail.com', CAST(N'2016-06-23 20:34:01.450' AS DateTime), 0, 4, CAST(N'2016-06-23 20:34:01.450' AS DateTime), N'(541) 754-3010', N'1-408-999 8888', N'3')
GO
INSERT [dbo].[Users] ([Id], [UserName], [Password], [TenantUnitId], [Email], [DateCreated], [Deactivated], [RoleId], [LastSignIn], [Phone], [Fax], [EmployeeId]) VALUES (5, N'Dennis Skoulz', N'AABB2100033F0352FE7458E412495148', 4, N'dennisskoulz@gmail.com', CAST(N'2016-06-23 20:34:01.450' AS DateTime), 0, 5, CAST(N'2016-06-23 20:34:01.450' AS DateTime), N'(541) 744-5010', N'1-418-999 8888', N'4')
GO
INSERT [dbo].[Users] ([Id], [UserName], [Password], [TenantUnitId], [Email], [DateCreated], [Deactivated], [RoleId], [LastSignIn], [Phone], [Fax], [EmployeeId]) VALUES (6, N'Rayan Giggz', N'AABB2100033F0352FE7458E412495148', 4, N'ryangiggz@gmail.com', CAST(N'2016-06-23 17:34:01.453' AS DateTime), 1, 5, NULL, N'(541) 744-6010', N'1-418-999 8888', N'5')
GO
INSERT [dbo].[Users] ([Id], [UserName], [Password], [TenantUnitId], [Email], [DateCreated], [Deactivated], [RoleId], [LastSignIn], [Phone], [Fax], [EmployeeId]) VALUES (7, N'Victoria Gomez', N'AABB2100033F0352FE7458E412495148', 4, N'viki@gmail.com', CAST(N'2016-06-23 17:34:01.453' AS DateTime), 0, 5, NULL, NULL, NULL, N'6')
GO
INSERT [dbo].[Users] ([Id], [UserName], [Password], [TenantUnitId], [Email], [DateCreated], [Deactivated], [RoleId], [LastSignIn], [Phone], [Fax], [EmployeeId]) VALUES (8, N'Paul Philips', N'AABB2100033F0352FE7458E412495148', 3, N'pylpav@hotmail.com', CAST(N'2016-06-27 12:10:37.397' AS DateTime), 0, 4, NULL, N'', N'', N'')
GO
INSERT [dbo].[Users] ([Id], [UserName], [Password], [TenantUnitId], [Email], [DateCreated], [Deactivated], [RoleId], [LastSignIn], [Phone], [Fax], [EmployeeId]) VALUES (9, N'Basil Hamilton', N'AABB2100033F0352FE7458E412495148', 3, N'basilhamilton@gmail.com', CAST(N'2016-06-27 13:04:46.317' AS DateTime), 0, 4, NULL, N'', N'', N'11')
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
-- End Users

--Update Article

--UPDATE [Articles] 
--SET Parameter1 = 'Article Number ' + Convert(varchar(30),Id), Parameter2 = 'Color ' + Convert(varchar(30),Id), Parameter3 = 'Sz ' + Convert(varchar(30),Id)

--End Update Article



















