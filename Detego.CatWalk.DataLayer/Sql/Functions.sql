﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Function [dbo].[CsvToInt] ( @Array nvarchar(1000)) 
returns @IntTable table 
	(IntValue int)
AS
begin

	declare @separator char(1)
	set @separator = ','

	declare @separator_position int 
	declare @array_value nvarchar(1000) 
	
	set @array = @array + ','
	
	while patindex('%,%' , @array) <> 0 
	begin
	
	  select @separator_position =  patindex('%,%' , @array)
	  select @array_value = left(@array, @separator_position - 1)
	
		Insert @IntTable
		Values (Cast(@array_value as int))

	  select @array = stuff(@array, 1, @separator_position, '')
	end

	return
end
GO


CREATE Function [dbo].[GetArticleGtins] ( @articleId INT) 
returns nvarchar(MAX)
AS
begin
    DECLARE @gtinList NVARCHAR(MAX) = '';

	SELECT @gtinList = @gtinList + ';' + Gtin
	FROM Products Where ArticleId = @articleId

	return RIGHT(@gtinList, Len(@gtinList)-1);
end
GO


CREATE PROCEDURE [dbo].[StockTakeReport]
	@StockTakeIds nvarchar(MAX)
AS
BEGIN
	SET NOCOUNT ON;

select Articles.Id, Articles.GtinList, Articles.Parameters, count(*) as Quantity
from StockTakeEPCItems 
join Products on StockTakeEPCItems.ProductId = Products.Id
join Articles on Products.ArticleId = Articles.Id
where StockTakeEPCItems.StockTakeId in (select * from dbo.CsvToInt(@StockTakeIds))
group by Articles.Id, Articles.GtinList, Articles.Parameters
END

GO


Create FUNCTION [dbo].[IsLocked]
(
	-- Add the parameters for the function here
	@TableToCheck nvarchar(50)
)
RETURNS bit
AS
BEGIN

DECLARE @result bit

select @result = count(*) from
(
select object_name(P.object_id) as TableName
FROM   sys.dm_tran_locks   AS L
       join sys.partitions AS P 
        on L.resource_associated_entity_id = p.hobt_id
where object_name(P.object_id) = @TableToCheck
) as T

if @result > 0
  return 1
return 0
END
GO


Create PROCEDURE [dbo].[TryDelete]
AS
BEGIN
	declare @UsersDeleted bit = 0
	declare @StockTakesDeleted bit = 0
	declare @ArticlesDeleted bit = 0
	declare @ProductsDeleted bit = 0
	declare @TenantOperationsDeleted bit = 0

	if (dbo.IsLocked('StockTakeEPCItems') = 0)
	begin
		delete from StockTakeEPCItems 
		where StockTakeEPCItems.StockTakeId in (select StockTakes.Id from StockTakes where TenantUnitId in (select TenantUnits.Id from TenantUnits where TenantUnits.Deleted = 1))

		if (dbo.IsLocked('StockTakes') = 0)
		begin
			delete from StockTakes 
			where StockTakes.TenantUnitId in (select TenantUnits.Id from TenantUnits where TenantUnits.Deleted = 1)
			set @StockTakesDeleted = 1

			if (dbo.IsLocked('Users') = 0)
			begin
				delete from Users 
				where Users.TenantUnitId in (select TenantUnits.Id from TenantUnits where TenantUnits.Deleted = 1)
				set @UsersDeleted = 1
			end
		end

		if (dbo.IsLocked('Products') = 0)
		begin
			delete from Products 
			where Products.TenantUnitId in (select TenantUnits.Id from TenantUnits where TenantUnits.Deleted = 1)
			set @ProductsDeleted = 1

			if (dbo.IsLocked('Articles') = 0)
			begin
				delete from Articles 
				where Articles.TenantUnitId in (select TenantUnits.Id from TenantUnits where TenantUnits.Deleted = 1)
				set @ArticlesDeleted = 1

				if (dbo.IsLocked('ParameterNames') = 0)
				begin
					delete from ParameterNames 
					where ParameterNames.Id not in (select distinct ParameterNamesId from Articles)
				end
			end
		end
	end

	if (dbo.IsLocked('TenantOperations') = 0)
	begin
		delete from TenantOperations 
		where TenantOperations.TenantUnitId in (select TenantUnits.Id from TenantUnits where TenantUnits.Deleted = 1)
		set @TenantOperationsDeleted = 1
	end

	if (dbo.IsLocked('TenantUnits') = 0 and @UsersDeleted = 1 and @StockTakesDeleted = 1 and @ArticlesDeleted = 1 and @ProductsDeleted = 1 and @TenantOperationsDeleted = 1)
	begin
		delete from TenantUnits 
		where TenantUnits.Deleted = 1

		if (dbo.IsLocked('Addresses') = 0)
		begin
			delete from Addresses 
			where Addresses.Id not in (select TenantUnits.AddressId from TenantUnits where TenantUnits.AddressId is not null)
		end
	end
END
GO






