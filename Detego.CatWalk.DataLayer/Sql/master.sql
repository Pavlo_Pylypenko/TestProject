﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[StartBackgroundDelete]
AS
BEGIN
    SET NOCOUNT ON;
    declare @timeToRun nvarchar(50)
    set @timeToRun = '01:00:00'

    while 1 = 1
    begin
        waitfor delay @timeToRun
        begin
            execute [DetegoCatWalkDb].[dbo].[TryDelete];
        end
    end
END
GO


sp_procoption    @ProcName = 'StartBackgroundDelete',
                @OptionName = 'startup',
                @OptionValue = 'on'
GO