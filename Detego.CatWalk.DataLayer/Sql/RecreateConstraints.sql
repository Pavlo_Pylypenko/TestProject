
  IF OBJECT_ID(N'[dbo].[FK_Articles_ParameterNames]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[Articles] DROP CONSTRAINT [FK_Articles_ParameterNames];
  GO
  IF OBJECT_ID(N'[dbo].[FK_ProductArticle]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[Products] DROP CONSTRAINT [FK_ProductArticle];
  GO
  IF OBJECT_ID(N'[dbo].[FK_StockTakeEPCItems_Products]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[StockTakeEPCItems] DROP CONSTRAINT [FK_StockTakeEPCItems_Products];
  GO
  IF OBJECT_ID(N'[dbo].[FK_StockTakeEPCItems_StockTakes]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[StockTakeEPCItems] DROP CONSTRAINT [FK_StockTakeEPCItems_StockTakes];
  GO
  IF OBJECT_ID(N'[dbo].[FK_StockTakes_TenantUnits]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[StockTakes] DROP CONSTRAINT [FK_StockTakes_TenantUnits];
  GO
  IF OBJECT_ID(N'[dbo].[FK_StockTakes_Users]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[StockTakes] DROP CONSTRAINT [FK_StockTakes_Users];
  GO
  IF OBJECT_ID(N'[dbo].[FK_TenantUnitArticle]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[Articles] DROP CONSTRAINT [FK_TenantUnitArticle];
  GO
  IF OBJECT_ID(N'[dbo].[FK_TenantUnits_Addresses]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[TenantUnits] DROP CONSTRAINT [FK_TenantUnits_Addresses];
  GO
  IF OBJECT_ID(N'[dbo].[FK_TenantUnits_TenantUnits]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[TenantUnits] DROP CONSTRAINT [FK_TenantUnits_TenantUnits];
  GO
  IF OBJECT_ID(N'[dbo].[FK_Users_Roles]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Roles];
  GO
  IF OBJECT_ID(N'[dbo].[FK_Users_TenantUnits]', 'F') IS NOT NULL
      ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_TenantUnits];
  GO
  


  ALTER TABLE [dbo].[TenantUnits]
  ADD CONSTRAINT [FK_TenantUnits_Addresses]
      FOREIGN KEY ([AddressId])
      REFERENCES [dbo].[Addresses]
          ([Id])
      ON DELETE CASCADE ON UPDATE NO ACTION;
  GO
  


  ALTER TABLE [dbo].[Articles]
  ADD CONSTRAINT [FK_Articles_ParameterNames]
      FOREIGN KEY ([ParameterNamesId])
      REFERENCES [dbo].[ParameterNames]
          ([Id])
      ON DELETE CASCADE ON UPDATE NO ACTION;
  GO
  

  ALTER TABLE [dbo].[Products]
  ADD CONSTRAINT [FK_ProductArticle]
      FOREIGN KEY ([ArticleId])
      REFERENCES [dbo].[Articles]
          ([Id])
      ON DELETE CASCADE ON UPDATE NO ACTION;
  GO
  

  ALTER TABLE [dbo].[Articles]
  ADD CONSTRAINT [FK_TenantUnitArticle]
      FOREIGN KEY ([TenantUnitId])
      REFERENCES [dbo].[TenantUnits]
          ([Id])
      ON DELETE CASCADE ON UPDATE NO ACTION;
  GO

  ALTER TABLE [dbo].[StockTakeEPCItems]
  ADD CONSTRAINT [FK_StockTakeEPCItems_Products]
      FOREIGN KEY ([ProductId])
      REFERENCES [dbo].[Products]
          ([Id])
      ON DELETE CASCADE ON UPDATE NO ACTION;
  GO
  

  ALTER TABLE [dbo].[Users]
  ADD CONSTRAINT [FK_Users_Roles]
      FOREIGN KEY ([RoleId])
      REFERENCES [dbo].[Roles]
          ([Id])
      ON DELETE CASCADE ON UPDATE NO ACTION;
  GO

  ALTER TABLE [dbo].[StockTakeEPCItems]
  ADD CONSTRAINT [FK_StockTakeEPCItems_StockTakes]
      FOREIGN KEY ([StockTakeId])
      REFERENCES [dbo].[StockTakes]
          ([Id])
      ON DELETE CASCADE ON UPDATE NO ACTION;
  GO

  ALTER TABLE [dbo].[StockTakes]
  ADD CONSTRAINT [FK_StockTakes_TenantUnits]
      FOREIGN KEY ([TenantUnitId])
      REFERENCES [dbo].[TenantUnits]
          ([Id])
      ON DELETE NO ACTION ON UPDATE NO ACTION;
  GO

  ALTER TABLE [dbo].[StockTakes]
  ADD CONSTRAINT [FK_StockTakes_Users]
      FOREIGN KEY ([UserId])
      REFERENCES [dbo].[Users]
          ([Id])
      ON DELETE CASCADE ON UPDATE NO ACTION;
  GO
  
  
  ALTER TABLE [dbo].[TenantUnits]
  ADD CONSTRAINT [FK_TenantUnits_TenantUnits]
      FOREIGN KEY ([ParentId])
      REFERENCES [dbo].[TenantUnits]
          ([Id])
      ON DELETE NO ACTION ON UPDATE NO ACTION;
  GO
  

  ALTER TABLE [dbo].[Users]
  ADD CONSTRAINT [FK_Users_TenantUnits]
      FOREIGN KEY ([TenantUnitId])
      REFERENCES [dbo].[TenantUnits]
          ([Id])
      ON DELETE NO ACTION ON UPDATE NO ACTION;
  GO
  
