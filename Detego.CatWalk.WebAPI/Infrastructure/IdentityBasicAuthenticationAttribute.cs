﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using Detego.CatWalk.BusinessLayer.Helpers;
using Microsoft.AspNet.Identity.Owin;

namespace Detego.CatWalk.WebAPI.Infrastructure
{
    public class IdentityBasicAuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple { get { return false; } }

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            // 1. Look for credentials in the request.
            HttpRequestMessage request = context.Request;
            AuthenticationHeaderValue authorization = request.Headers.Authorization;

            // 2. If there are no credentials, do nothing.
            if (authorization == null)
            {
                return;
            }

            // 3. If there are credentials but the filter does not recognize the 
            //    authentication scheme, do nothing.
            if (authorization.Scheme != "Bsc" && authorization.Scheme != "Basic")
            {
                return;
            }

            // 4. If there are credentials that the filter understands, try to validate them.
            // 5. If the credentials are bad, set the error result.
            if (String.IsNullOrEmpty(authorization.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
                return;
            }

            Tuple<string, string> userNameAndPasword = ExtractUserNameAndPassword(authorization.Parameter);
            if (userNameAndPasword == null)
            {
                context.ErrorResult = new AuthenticationFailureResult("Invalid credentials", request);
            }

            string userName = userNameAndPasword.Item1;
            string password = userNameAndPasword.Item2;

            var error = new ErrorMessageWrapper();
            IPrincipal principal = await AuthenticateAsync(userName, password, error, cancellationToken);

            if (principal == null)
            {
                context.ErrorResult = new AuthenticationFailureResult(error.Error, request);
            }
            // 6. If the credentials are valid, set principal.
            else
            {
                context.Principal = principal;
            }
        }

        private static Tuple<string, string> ExtractUserNameAndPassword(string authorizationParameter)
        {
            byte[] credentialBytes;

            try
            {
                credentialBytes = Convert.FromBase64String(authorizationParameter);
            }
            catch (FormatException)
            {
                return null;
            }

            Encoding encoding = Encoding.ASCII;
            encoding = (Encoding)encoding.Clone();
            encoding.DecoderFallback = DecoderFallback.ExceptionFallback;
            string decodedCredentials;

            try
            {
                decodedCredentials = encoding.GetString(credentialBytes);
            }
            catch (DecoderFallbackException)
            {
                return null;
            }

            if (String.IsNullOrEmpty(decodedCredentials))
            {
                return null;
            }

            int colonIndex = decodedCredentials.IndexOf(':');

            if (colonIndex == -1)
            {
                return null;
            }

            string userName = decodedCredentials.Substring(0, colonIndex);
            string password = decodedCredentials.Substring(colonIndex + 1);
            return new Tuple<string, string>(userName, password);
        }

        private async Task<IPrincipal> AuthenticateAsync(string userName, string password, ErrorMessageWrapper error, CancellationToken cancellationToken)
        {
            WebUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<WebUserManager>();

            cancellationToken.ThrowIfCancellationRequested();
            WebUser user = await userManager.FindByEmailAsync(userName);

            if (user.User == null)
            {
                error.Error = "Invalid user/password combination!";
                return null;
            }

            if (user.User.FailedLogins == null)
            {
                user.User.FailedLogins = 0;
                await userManager.UpdateAsync(user);
            }
            if (user.User.Deactivated)
            {
                error.Error = "User is deactivated";
                return null;
            }

            var hash = PasswordHelper.CalculateMD5Hash(password);
            if (hash != user.User.Password)
            {
                error.Error = "Invalid user/password combination!";
                user.User.FailedLogins += 1;
                var maxFailedLogins = ConfigurationManager.AppSettings["MaxFailedLogins"] == null ? 5 : int.Parse(ConfigurationManager.AppSettings["MaxFailedLogins"]);
                if (user.User.FailedLogins < maxFailedLogins)
                {
                    await userManager.UpdateAsync(user);
                    return null;
                }
                else
                    user.User.Deactivated = true;
            }
            if (user.User.FailedLogins != 0)
            {
                user.User.FailedLogins = 0;
                await userManager.UpdateAsync(user);
            }

            if (user.User.Deactivated)
            {
                error.Error = "User is deactivated";
                return null;
            }

            cancellationToken.ThrowIfCancellationRequested();
            ClaimsIdentity identity = await userManager.ClaimsIdentityFactory.CreateAsync(userManager, user, "Bsc");

            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, userName));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.User.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Role, user.User.Role.Name));
            claims.Add(new Claim(ClaimTypes.UserData, user.User.TenantUnitId.ToString()));
            identity.AddClaims(claims);

            var principal = new ClaimsPrincipal(identity);
            Thread.CurrentPrincipal = principal;
            HttpContext.Current.User = principal;

            return principal;
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            var challenge = new AuthenticationHeaderValue("Bsc");
            context.Result = new AddChallengeOnUnauthorizedResult(challenge, context.Result);
            return Task.FromResult(0);
        }
    }
}