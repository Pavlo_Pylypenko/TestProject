﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.DataLayer;
using Microsoft.AspNet.Identity;

namespace Detego.CatWalk.WebAPI.Infrastructure
{
    public class WebUserStore: IUserPasswordStore<WebUser, int>, IUserEmailStore<WebUser, int>, IUserRoleStore<WebUser, int>
    {
        private DetegoCatWalkDbContext _context;

        public WebUserStore(DetegoCatWalkDbContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }

        public Task CreateAsync(WebUser user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(WebUser user)
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                NLogLogger logger = new NLogLogger(null, "EditUser");

                foreach (var eve in e.EntityValidationErrors)
                {
                    logger.Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            return Task.FromResult(true);
        }

        public Task DeleteAsync(WebUser user)
        {
            throw new NotImplementedException();
        }

        public Task<WebUser> FindByIdAsync(int userId)
        {
            return Task.FromResult(new WebUser { User = _context.Users.FirstOrDefault(u => u.Id == userId) });
        }

        // Is called to find an user for update. UserName must be email.
        public Task<WebUser> FindByNameAsync(string userName)
        {
            return Task.FromResult(new WebUser { User = _context.Users.FirstOrDefault(u => u.Email == userName) });
        }

        public Task SetPasswordHashAsync(WebUser user, string passwordHash)
        {
            throw new NotImplementedException();
        }
    
        public Task<string> GetPasswordHashAsync(WebUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> HasPasswordAsync(WebUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailAsync(WebUser user, string email)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetEmailAsync(WebUser user)
        {
            return Task.FromResult(user.User.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(WebUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailConfirmedAsync(WebUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public Task<WebUser> FindByEmailAsync(string email)
        {
            return Task.FromResult(new WebUser { User = _context.Users.Include(u => u.Role).Include(u => u.TenantUnit).FirstOrDefault(u => u.Email == email && ((u.TenantUnit != null && !u.TenantUnit.Deleted) || u.TenantUnit == null)) });
        }

        public Task SetPhoneNumberAsync(WebUser user, string phoneNumber)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPhoneNumberAsync(WebUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(WebUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetPhoneNumberConfirmedAsync(WebUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public Task AddToRoleAsync(WebUser user, string roleName)
        {
            return null;
        }

        public Task RemoveFromRoleAsync(WebUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(WebUser user)
        {
            IList<string> list = new List<string>();
            list.Add(user.User.Role.Name);
            return Task.FromResult(list);
        }

        public Task<bool> IsInRoleAsync(WebUser user, string roleName)
        {
            throw new NotImplementedException();
        }
    }
}