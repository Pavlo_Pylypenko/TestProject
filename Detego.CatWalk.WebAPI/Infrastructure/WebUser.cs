﻿using System.Security.Claims;
using System.Threading.Tasks;
using Detego.CatWalk.DataLayer;
using Microsoft.AspNet.Identity;

namespace Detego.CatWalk.WebAPI.Infrastructure
{
    public class WebUser : IUser<int>
    {
        public User User { get; set; }

        public int Id
        {
            get { return User.Id; }
        }

        public string UserName
        {
            get
            {
                return User.Email;
            }
            set { User.Email = value; }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(WebUserManager manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
    }
}