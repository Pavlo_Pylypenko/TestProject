﻿using Detego.CatWalk.DataLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Detego.CatWalk.WebAPI.Infrastructure
{
    public class WebUserManager : UserManager<WebUser, int>
    {
        public WebUserManager(IUserStore<WebUser, int> store) : base(store)
        {
        }

        public static WebUserManager Create(IdentityFactoryOptions<WebUserManager> options, IOwinContext context) 
        {
            var manager = new WebUserManager(new WebUserStore(context.Get<DetegoCatWalkDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<WebUser, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3
            };

            return manager;
        }
    }
}