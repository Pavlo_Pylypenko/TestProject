﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.DataLayer;
using Detego.CatWalk.WebAPI.Models;

namespace Detego.CatWalk.WebAPI.Infrastructure
{
    public class LoggingFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            LogAction(actionContext, true);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            LogAction(actionExecutedContext.ActionContext, false);
        }

        private void LogAction(HttpActionContext actionContext, bool isStart)
        {
            int? userId = null;

            StringBuilder message = new StringBuilder(string.Format(@"{0}", isStart? "Request":"Response"));

            if (isStart && actionContext.ActionArguments.Count > 0)
            {
                foreach (KeyValuePair<string,object> arg in actionContext.ActionArguments)
                {

                    if (IsValueType(arg.Value))
                    {
                        message.AppendFormat(" {0} = {1},", arg.Key, arg.Value);
                    }
                    else
                    {
                        DisplayObject(arg, message);        
                    }
                    
                }
                if (actionContext.ActionArguments.Count > 0)
                {
                    message.Remove(message.Length - 1, 1);
                }
            }

            if (!isStart)
            {
                if (actionContext.Response != null)
                {
                    message.AppendFormat(" StatusCode = {0}", actionContext.Response.StatusCode);
                    if (!actionContext.Response.IsSuccessStatusCode)
                    {
                        message.AppendFormat(" Content = {0} ", actionContext.Response.Content);
                    } 
                }
            }

            if ((actionContext.ControllerContext.RouteData.Values.ContainsKey("user")) && (actionContext.ControllerContext.RouteData.Values["user"] != null))
                userId = Convert.ToInt32(actionContext.ControllerContext.RouteData.Values["user"]);

            NLogLogger logger = new NLogLogger(userId, string.Format(@"{0}\{1} Method={2} ", actionContext.ActionDescriptor.ControllerDescriptor.ControllerName, actionContext.ActionDescriptor.ActionName, actionContext.Request.Method));
            logger.Info(message.ToString());
        }

        private void DisplayObject(KeyValuePair<string, object> arg, StringBuilder message)
        {
            if (arg.Key == "sti")
            {
                //ParseObject(arg.Value, message);
                StockTakeInfo sti = (arg.Value as StockTakeInfo);
                message.AppendFormat(" Site = {0},", sti.Site);
                message.AppendFormat(" Location = {0},", sti.Location);
                message.AppendFormat(" Readpoint = {0},", sti.Readpoint);
                message.AppendFormat(" UserEmail = {0},", sti.UserEmail);
                message.AppendFormat(" TimeZone = {0},", sti.TimeZone);
                message.AppendFormat(" TimeStamp = {0},", sti.TimeStamp);
                message.AppendFormat(" EPCList.Count = {0},", sti.EpcList.Count);
            }
            else
            {
                if (arg.Key == "value")
                {
                    message.AppendFormat(" input value = {0},", arg.Value); 
                }

                if (arg.Value is TenantUnit)
                {
                    message.AppendFormat(" tenant Id = {0},", (arg.Value as TenantUnit).Id);
                    message.AppendFormat(" tenant name = {0},", (arg.Value as TenantUnit).Name);
                    return;
                }

                if (arg.Value is User)
                {   
                    message.AppendFormat(" user Id = {0},", (arg.Value as User).Id);
                    message.AppendFormat(" user name = {0},", (arg.Value as User).UserName);
                    message.AppendFormat(" user email = {0},", (arg.Value as User).Email);
                    message.AppendFormat(" user tenant Id = {0},", (arg.Value as User).TenantUnitId);
                    return;
                }

                if (arg.Key=="stockTakes" && arg.Value is IEnumerable<int>)
                {
                    List<int> list = arg.Value as List<int>;
                    if (list != null && list.Count() >= 2)
                    {
                        message.Append(" Ids of Stock Takes to Combine: ");
                        foreach (var id in list)
                        {
                            message.AppendFormat("{0},", id);
                        }
                    }
                }
            }
        }

        public bool IsValueType(object obj)
        {
            return obj != null && (obj.GetType().IsValueType || obj.GetType().Name == "String");
        }
    }
}