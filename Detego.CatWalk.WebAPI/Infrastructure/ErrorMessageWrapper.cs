﻿namespace Detego.CatWalk.WebAPI.Infrastructure
{
    public class ErrorMessageWrapper
    {
        public string Error { get; set; }
    }
}