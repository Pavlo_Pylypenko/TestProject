﻿using System;

namespace Detego.CatWalk.WebAPI.Models
{
    public class UserView
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int LabelId { get; set; }
        public string LabelName { get; set; }
        public int TenantUnitId { get; set; }
        public string TenantUnitName { get; set; }
        public string Email { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? LastSignIn { get; set; }
        public bool Deactivated { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string EmployeeId { get; set; }
    }
}