﻿using System;
using System.Collections.Generic;

namespace Detego.CatWalk.WebAPI.Models
{
    public class StockTakeInfo
    {
        public string Site { get; set; }

        public string Location { get; set; }

        public string Readpoint { get; set; }

        public string UserEmail { get; set; }

        public List<string> EpcList { get; set; }

        public int TimeZone { get; set; }

        public DateTime? TimeStamp { get; set; }

    }
}