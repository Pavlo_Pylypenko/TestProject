﻿using Detego.CatWalk.DataLayer;
using Detego.CatWalk.WebAPI;
using Detego.CatWalk.WebAPI.Infrastructure;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Detego.CatWalk.WebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(() => new DetegoCatWalkDbContext());
            app.CreatePerOwinContext<WebUserManager>(WebUserManager.Create);
        }
    }
}