﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.DataLayer;
using Detego.CatWalk.WebAPI.Infrastructure;
using Ionic.Zip;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class MasterDataController : BaseApiController
    {
        private readonly IMasterDataManager _masterDataManager = new MasterDataManager();

        [HttpPost]
        public async void ImportMasterData()
        {
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.UnsupportedMediaType));

            try
            {
                var task = Request.Content.LoadIntoBufferAsync();
                task.Wait();
                var formData = await Request.Content.ReadAsMultipartAsync();
                var timeZone = int.Parse(formData.Contents[3].ReadAsStringAsync().Result);
                var sku = formData.Contents[2].ReadAsStringAsync().Result;
                var tenantId = formData.Contents[1].ReadAsStringAsync().Result;
                var fileStream = formData.Contents[0].ReadAsStreamAsync().Result;

                Task.Run(() =>
                {
                    _masterDataManager.ImportMasterData(Convert.ToInt32(tenantId), fileStream, sku, timeZone);
                });
            }
            catch (Exception)
            {
                NLogLogger logger = new NLogLogger(null, @"MasterData\ImportMasterData Method=POST");
                logger.Error("Master Data wasn't imported");
            }
        }

        [HttpGet]
        public TenantOperation IsMasterDataImporting(int tenantId)
        {
            return _masterDataManager.IsMasterDataImporting(tenantId);
        }

        [HttpGet]
        public List<string> GetMasterDataBySite(string gln)
        {
            return _masterDataManager.GetMasterDataBySite(gln);
        }

        [HttpPost]
        public HttpResponseMessage ImportImages()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files["file"];
                int tenantId = int.Parse(HttpContext.Current.Request.Form["tenantId"]);

                if (httpPostedFile != null)
                {
                    var fileSavePath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~\\App_Data"), Path.GetFileName(httpPostedFile.FileName));
                    try
                    {
                        httpPostedFile.SaveAs(fileSavePath);
                        using (var zip = ZipFile.Read(fileSavePath))
                        {
                            if (zip.Entries.Select(e => Path.GetExtension(e.FileName)).Count(e => e == ".jpg" || e == ".jpeg" || e == ".png") == 0)
                                return Request.CreateResponse(HttpStatusCode.OK, "noimage");
                        }
                    }
                    catch (Exception ex)
                    {
                        NLogLogger logger = new NLogLogger(null, @"MasterData\ImportImages Method=POST");
                        logger.Error("Images wasn't uploaded reason: " + NLogLogger.GetException(ex).Message);
                    }

                    Task.Run(() =>
                    {
                        _masterDataManager.ImportImages(tenantId, fileSavePath);
                    });
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public List<string> GetImportedColumns(int tenantId)
        {
            var result = _masterDataManager.GetColumns(tenantId);
            return result;
        }

        public string GetSkuColumns(int tenantId)
        {
            var result = _masterDataManager.GetFirstArticleForTenant(tenantId);
            if (result != null)
                return result.ParameterName.SkuColumns;
            return String.Empty;
        }
    }
}
