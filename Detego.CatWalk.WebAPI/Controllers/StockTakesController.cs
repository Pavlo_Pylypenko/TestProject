﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.BusinessLayer.Models;
using Detego.CatWalk.DataLayer;
using Detego.CatWalk.WebAPI.Infrastructure;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class StockTakesController : BaseApiController
    {
        private IStockTakeManager _stockTakeManager = new StockTakeManager();

        [HttpPost]
        public void CombineStockTake(List<int> stockTakes)
        {
            try
            {
                Task.Run(() =>
                {
                    _stockTakeManager.CombineStockTakes(stockTakes);
                    NLogLogger logger = new NLogLogger(null, @"StockTakes\CombineStockTake Method=POST");
                    logger.Info("Stock Takes were combined");
                });
            }
            catch (Exception)
            {
                // ignored
                NLogLogger logger = new NLogLogger(null, @"StockTakes\CombineStockTake Method=POST");
                logger.Error("Stock Takes were not combined");
            }
        }

        [Route("api/stocktakes/{id}")]
        public List<StockTake> GetStockTakes([FromUri(Name="id")]int tenantId)
        {
            return _stockTakeManager.GetStockTakes(tenantId);
        }

        [Route("api/stocktakes/site/{id}")]
        public List<StockTakesByDate> GetStockTakesByDate([FromUri(Name = "id")]int siteId)
        {
            return _stockTakeManager.GetStockTakesByDate(siteId);
        }
    }
}
