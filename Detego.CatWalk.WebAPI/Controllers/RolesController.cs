﻿using System.Collections.Generic;
using System.Linq;
using Detego.CatWalk.BusinessLayer.Helpers;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.DataLayer;
using Detego.CatWalk.WebAPI.Infrastructure;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class RolesController : BaseApiController
    {
        private static readonly IUserManager _userManager = new UserManager();

        public List<Role> GetRoles()
        {
            return _userManager.GetRoles();
        }

        public List<Role> getRole(string id)
        {
            var result = RoleHelper.GetLessRoles(id);
            return result.ToList();
        }
    }
}