﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.WebAPI.Infrastructure;
using Detego.CatWalk.WebAPI.Models;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class StockTakeController : BaseApiController
    {
        private IStockTakeManager _stockTakeManager = new StockTakeManager();

        [HttpPost]
        public void InsertStockTake(StockTakeInfo sti)
        {
            //{"Site":"0000000001000","Location":"Location 1","Readpoint":"handheld0","UserEmail":"root@mail.com","EpcList":[ "3034F77AD406F69000012B1F","3034F77AD406F69000002E07","3034F77FA4195CD00001298F", "3034F77AD406F69000012B24"], "TimeZone": 3, "TimeStamp": "2016-09-12"}          
            //{"Site":"0000000001000","Location":"Location 1","Readpoint":"handheld0","UserEmail":"root@mail.com","EpcList":[ "3034F77FA4195CD00001298F", "3034F77AD406F69000012B24","3034F784BC14C81000012996","3034F77AD406F69000012B23", "3034F77AD406F69000012B22","3034F77AD406F69000012B21"], "TimeZone": 3, "TimeStamp": "2016-09-12"}    
            try
            {
                Task.Run(() =>
                {
                    bool isInserted = _stockTakeManager.InsertStockTake(sti.Site, sti.Location, sti.EpcList, sti.Readpoint, sti.UserEmail, sti.TimeZone, sti.TimeStamp);
                });
            }
            catch (Exception)
            {
                // ignored
                NLogLogger logger = new NLogLogger(null, @"StockTake\InsertStockTake Method=POST");
                logger.Error("Stock Take wasn't inserted");
            }
        }

        [HttpGet]
        public void EndStockTake(string site)
        {
            _stockTakeManager.EndStockTake(site);
        }

    }
}
