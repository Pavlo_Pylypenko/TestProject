﻿using System.Collections.Generic;
using System.Web.Http;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.WebAPI.Infrastructure;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class ProductController : BaseApiController
    {
        private static readonly IMasterDataManager _masterDataManager = new MasterDataManager();

        [HttpGet]
        public List<string> CompanyPrefixBySite(string gln)
        {
            return _masterDataManager.GetCompanyPrefixBySite(gln);
        }
    }
}
