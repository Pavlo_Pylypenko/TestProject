﻿using System;
using System.Web.Http;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.BusinessLayer.Models;
using Detego.CatWalk.WebAPI.Infrastructure;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class ReportsController: BaseApiController
    {
        private IReportManager _reportManager = new ReportManager();
    
        [Route("api/Reports/{stocktakeid}")]
        [DeflateCompressionAttribute]
        public StockTakeReportData GetStockTake(int stockTakeId)
        {
            StockTakeReportData result = _reportManager.GetReport(stockTakeId);
            return result;
        }

        [Route("api/Reports/{siteid}/{date}")]
        [DeflateCompressionAttribute]
        public StockTakeReportData GetStockTake(int siteId, DateTime date)
        {
            StockTakeReportData result = _reportManager.GetReport(siteId, date);
            return result;
        }
    }
}