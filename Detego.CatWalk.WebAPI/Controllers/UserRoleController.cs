﻿using Detego.CatWalk.BusinessLayer.Helpers;
using Detego.CatWalk.WebAPI.Infrastructure;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class UserRoleController : BaseApiController
    {
        public string GetCurrentUserRole()
        {
            var role =  User.GetRole();
            return role;
        }
    }
}
