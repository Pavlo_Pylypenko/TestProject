﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Detego.CatWalk.BusinessLayer.Helpers;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.DataLayer;
using Detego.CatWalk.WebAPI.Infrastructure;
using Detego.CatWalk.WebAPI.Models;
using Microsoft.AspNet.Identity.Owin;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class UsersController : BaseApiController
    {
        private static readonly IUserManager _userManager = new UserManager();
        private static readonly ITenantManager _tenantManager = new TenantManager(); 

        [DeflateCompression]
        public IEnumerable<UserView> GetUserList()
        {
            var userList = _userManager.GetUserList(User.GetId(), User.GetTenantId());
            foreach (var user in userList)
            {
                var label = _tenantManager.GetUserLabel(user);
                yield return new UserView {DateCreated = user.DateCreated, Deactivated = user.Deactivated, Email = user.Email, EmployeeId = user.EmployeeId, Fax = user.Fax, Id = user.Id, UserName = user.UserName, LastSignIn = user.LastSignIn, TenantUnitId = user.TenantUnit.Id, TenantUnitName = user.TenantUnit.Name, Phone = user.Phone, RoleId = user.Role.Id, RoleName = user.Role.Name, LabelId = label.Id, LabelName = label.Name};
            }
        }

        [ResponseType(typeof(User))]
        [Route("api/Users/{id}")]
        public IHttpActionResult GetUser([FromUri(Name = "id")]int userId)
        {
            return Ok(_userManager.GetUser(userId));
        }

        [HttpPost]
        [ResponseType(typeof(bool))]
        public IHttpActionResult AddUser(User user)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<WebUserManager>();
            var sameEmailUser = userManager.FindByEmailAsync(user.Email);
            if (sameEmailUser.Result.User != null && sameEmailUser.Result.User.Id != user.Id)
                return BadRequest("User with same email already exists.");

            var pass = PasswordHelper.RandomPassword();
            user.Password = PasswordHelper.CalculateMD5Hash(pass);
            int result = _userManager.AddUser(user);
            // To send non-hashed password in email
            Task.Run(() => EmailHelper.SendCreateUser(new User { UserName = user.UserName, Email = user.Email, Password = pass }));
            return Ok(result);
        }

        [HttpPut]
        [ResponseType(typeof(bool))]
        public IHttpActionResult EditUser(User user)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<WebUserManager>();
            var sameEmailUser = userManager.FindByEmailAsync(user.Email);
            if (sameEmailUser.Result.User != null && sameEmailUser.Result.User.Id != user.Id)
                return BadRequest("User with same email already exists.");

            bool isEmailChanged = _userManager.CheckUserEmailChanged(user);

            string pass = string.Empty;
            if (isEmailChanged)
            {
                pass = PasswordHelper.RandomPassword();
                user.Password = PasswordHelper.CalculateMD5Hash(pass); 
            }

            bool result = _userManager.EditUser(user);
            if (isEmailChanged)
            {
                Task.Run(() => EmailHelper.SendCreateUser(new User { UserName = user.UserName, Email = user.Email, Password = pass })); 
            }
            return Ok(result);
        }

        [HttpDelete]
        [ResponseType(typeof(bool))]
        [Route("api/Users/{id}")]
        public IHttpActionResult EnableDisableUser(int id)
        {
            bool result = _userManager.EnableDisableUser(id);
            return Ok(result);
        }
    }
}
