﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Detego.CatWalk.BusinessLayer.Helpers;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.DataLayer;
using Detego.CatWalk.WebAPI.Infrastructure;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class TenantsController : BaseApiController
    {
        private static readonly ITenantManager _tenantManager = new TenantManager();

        public TenantUnit[] GetRootTenants()
        {
            TenantUnit[] tenants;
            if (User.IsInRole(RoleHelper.Root))
                tenants = _tenantManager.GetRootTenants();
            else
                tenants = new [] { _tenantManager.GetRootTenant(User.GetTenantId()) };
             return tenants;
        }

        public TenantUnit[] GetChildTenants(int id)
        {
            TenantUnit[] tenants;
            if (User.IsInRole(RoleHelper.Root))
                tenants = _tenantManager.GetChildTenants(id);
            else
                tenants = _tenantManager.GetChildTenants(id, User.GetTenantId().Value);
            return tenants;
        }

        [HttpGet]
        [Route("api/retrievetenants/{id}")]
        public TenantUnit[] RetrieveTenants([FromUri(Name = "Id")]int tenantUnitType)
        {
                        
            var tenants = _tenantManager.GetTenants(this.User.GetTenantId(), tenantUnitType);
            return tenants;
        }

        [HttpGet]
        [Route("api/retrievetenants/{id}/{labelId}")]
        public TenantUnit[] RetrieveTenants([FromUri(Name = "Id")]int tenantUnitType, int labelId)
        {
            var tenants = _tenantManager.GetTenants(labelId, tenantUnitType);
            return tenants;
        }

        [HttpPost]
        public HttpResponseMessage AddTenant(TenantUnit tenant)
        {
            int result = _tenantManager.AddTenant(tenant);
            if (result == -1)
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error adding new tenant.");
            if (result == -2 || result == -3)
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPut]
        public HttpResponseMessage EditTenant(TenantUnit tenant)
        {
            int result = _tenantManager.EditTenant(tenant);
            if (result == -1)
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error editing new tenant.");
            if (result == -2 || result == -3)
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("api/tenants/{id}")]
        public bool DeleteTenant(int id)
        {
            return _tenantManager.DeleteTenant(id);
        }
    }
}
