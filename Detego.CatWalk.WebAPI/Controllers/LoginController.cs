﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Detego.CatWalk.BusinessLayer.Helpers;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.WebAPI.Infrastructure;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;

namespace Detego.CatWalk.WebAPI.Controllers
{
    public class LoginController : ApiController
    {
        IUserManager _userManager = new UserManager();

        [IdentityBasicAuthentication]
        [Authorize]
        [HttpGet]
        public HttpResponseMessage Login()
        {
            var currentUser =_userManager.GetUser(User.GetId());
            var lastSignIn = currentUser.LastSignIn;
            currentUser.LastSignIn = DateTime.Now;
            _userManager.EditUser(currentUser);

            if (lastSignIn == null)
                return Request.CreateResponse(HttpStatusCode.OK, currentUser.Password);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public HttpResponseMessage ResetPassword(string url, string email)
        {   
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<WebUserManager>();
            var task = userManager.FindByEmailAsync(email);
            if (task.Result.User != null && !task.Result.User.Deactivated)
            {
                Task.Run(() => EmailHelper.SendPasswordReset(task.Result.User, url));
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                if (task.Result.User != null && task.Result.User.Deactivated)
                {
                   return Request.CreateResponse(HttpStatusCode.InternalServerError, "User is deactivated"); 
                }

                return Request.CreateResponse(HttpStatusCode.InternalServerError, "User is not found");
            }    
        }

        [HttpPost]
        public void ChangePassword([FromBody]JObject value)
        {
            dynamic json = value;
            string email = json.Email.ToObject<string>();
            string token = json.Token.ToObject<string>();
            string newPassword = json.NewPassword.ToObject<string>();
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<WebUserManager>();
            Task<WebUser> task = userManager.FindByEmailAsync(email);
            task.Wait();
            var user = task.Result.User;
            if (user != null && user.Password == token)
            {
                _userManager.ChangePassword(email, newPassword);
                Task.Run(() => EmailHelper.SendPasswordChanged(user, newPassword));
            }
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
    }
}