﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Detego.CatWalk.WebAPI.Infrastructure;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [LoggingFilter]
    [Authorize]
    public class BaseApiController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
    }
}