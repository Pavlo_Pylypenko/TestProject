﻿using System.Collections.Generic;
using System.Web.Http;
using Detego.CatWalk.BusinessLayer.Interfaces;
using Detego.CatWalk.BusinessLayer.Managers;
using Detego.CatWalk.BusinessLayer.Models;
using Detego.CatWalk.WebAPI.Infrastructure;

namespace Detego.CatWalk.WebAPI.Controllers
{
    [IdentityBasicAuthentication]
    public class SitesController : BaseApiController
    {
        private ITenantManager _tenantManager = new TenantManager();

        [HttpGet]
        public Site GetSite(string gln)
        {
            return  _tenantManager.GetSite(gln);
        }

        [Route("api/UserSites")]
        [HttpGet]
        public List<string> GetSitesByUser(string userEmail)
        {
            return _tenantManager.GetSiteByUser(userEmail);
        }
    }
}
