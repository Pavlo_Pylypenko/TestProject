﻿using System;
using System.Web.Http;
using Detego.CatWalk.Test.Model;
using Detego.CatWalk.Test.Tests;

namespace Detego.CatWalk.TestAPI.Controllers
{
    public class PerformanceTestsController : ApiController
    {
        PerformanceTests _tests = new PerformanceTests();

        [HttpGet]
        public PerformanceTestResult Test1StockTakeOnAzure()
        {
            return _tests.Test1StockTake();
        }

        [HttpGet]
        public PerformanceTestResult Test10StockTakesOnAzure()
        {
            return _tests.Test10StockTakes();
        }

        [HttpGet]
        public PerformanceTestResult Test1StockTakeNoGtinOnAzure()
        {
            return _tests.Test1StockTakeNoGtin();
        }

        [HttpGet]
        public PerformanceTestResult Test10StockTakesNoGtinOnAzure()
        {
            return _tests.Test10StockTakesNoGtin();
        }

        [HttpGet]
        public DateTime TestDate()
        {
            return DateTime.Now;
        }
    }
}